int STRIPE_COUNT = 55;
Stripe baseStripe;
StripeController stripeController;
PImage back;

void setup() { 
  size(640, 640);
  initStripes ();
}

void initStripes () {
  baseStripe = new Stripe ();
  baseStripe.myWidth = 18f;
  
  stripeController = new StripeController (baseStripe, STRIPE_COUNT);
}

void draw() {
  noStroke ();
  background (255);
  stripeController.onUpdate ();
}



