Timer switchTimer = new Timer (300);

void mousePressed() {
  if (switchTimer.isTime()) {
    stripeController.switchScenario ();
  }  
}

class StripeController {
  Stripe[] stripes;    
  float maxOffset;
  float distModif = 1f;
  float posOffset;
  Stripe base;
  int stripeIgnorePos = 0;
  
  StripeController (Stripe base, int count) {
    this.base = base;
    distModif = 0f;
    baseStripe.myWidth = 0f;
    this.stripes = new Stripe[count];
    for (int i = 0; i < stripes.length; i++) {
      stripes[i] = new Stripe (base);
    }
    posOffset = width / 2;
    maxOffset = width / Math.max (1, stripes.length) * 4.5f; 
    
    prepareScenarios ();
  }
  
  void onUpdate () {
    currentScenario.behave ();
    translate(posOffset, 0);  
    
    for (int i = 0; i < stripes.length; i++) {
      if (i < stripeIgnorePos) {
        continue;
      }
      float curX = calcX(i, stripes.length / 2);
      stripes[i].xPos = curX;
      stripes[i].clone(base);
      stripes[i].onDrawByStart ();
    }
  }
  
  float calcX (int i, int central) {  
    return  maxOffset * (i - central) * distModif;
  }
  
  ArrayList<StripeScenario> stripeScenarios = new ArrayList<StripeScenario> ();
  int currentScenarioNum;
  StripeScenario currentScenario;
   
  void switchScenario () {
    for (int i = 0; i < stripeScenarios.size(); i++) {
      if (stripeScenarios.get (i) == currentScenario) {
        int nextScenarioPosition = i + 1;
        if (nextScenarioPosition >= stripeScenarios.size()) {
          nextScenarioPosition = 0;
        } 
        currentScenario = stripeScenarios.get (nextScenarioPosition).onStart (this);
        break;
      }
    }
  }
  
  void prepareScenarios () {
    stripeScenarios.add( new VoidScenario ());
    stripeScenarios.add( new StripeCreationScenario ());
    stripeScenarios.add( new StripeMultiplicationScenario ());
    stripeScenarios.add( new StripeMovementScenario ());
    stripeScenarios.add( new StripeMovementWidthScenario ());
    stripeScenarios.add( new StripeDeplicationScenario ());

    currentScenario = stripeScenarios.get (0).onStart (this);
  }
}

class VoidScenario extends StripeScenario {
  void onBehave () {
    owner.distModif = lerp (owner.distModif, 0f, 0.15f);
    baseStripe.myWidth = lerp (baseStripe.myWidth, 0f, 0.4f);
    baseStripe.myHeight = height;
    baseStripe.yPos = lerp (baseStripe.yPos, -height, 0.1f);
  }
}

class StripeCreationScenario extends StripeScenario {  
  void initBehavs () {
    baseStripe.drawInfinite = false;
    owner.posOffset = width / 2;
    baseStripe.yPos = height;
    baseStripe.breakSize = 10f;
    owner.stripeIgnorePos = owner.stripes.length / 2;
  }
  
  void onBehave () {
    owner.posOffset = 0f;
    owner.distModif = lerp (owner.distModif, 0f, 0.15f);
    
    baseStripe.myWidth = step (baseStripe.myWidth, 2f, 0.5f);
    baseStripe.yPos -= 2.5f;
   
    if (baseStripe.yPos < -height * 3) {
      baseStripe.yPos = -height * 3;
    }   
 
    owner.posOffset = lerp (owner.posOffset, width / 2, 0.15f);
    translate(width, height / 2.5f);  
    rotate (radians (90));   
  }
}

class StripeMultiplicationScenario extends StripeScenario {
  void onBehave () {
    baseStripe.breakSize = 0f;
    baseStripe.yPos = 0f;
    baseStripe.breakSize = 0f;
    owner.distModif = lerp (owner.distModif, 1f, 0.015f);
    baseStripe.myWidth = lerp (baseStripe.myWidth, 2f, 0.05f);
     
    translate(width, height / 2.5f);  
    rotate (radians (90)); 
  }
}

class StripeMovementScenario extends StripeScenario {
  float rotationDesValue;
  float rotationStep = 0.5f;
  float widthTranslate;
  float heightTranslate;
  
  void initBehavs () {
     baseStripe.drawInfinite = true;
     rotationDesValue = 90;
     widthTranslate = width;
     heightTranslate = height / 2.5f;
     
     stripeBehavs.add (new StripeXOffsetBehaviour ().init (owner));
  }
  
  void onBehave () {
    owner.distModif = 1f;
    baseStripe.myWidth = 2f;
    baseStripe.breakSize = 0f;
    baseStripe.yPos = 0f;
    
    translate(widthTranslate, heightTranslate);  
    rotate (radians (rotationDesValue));
    
    rotationDesValue = step (rotationDesValue, 0f, rotationStep);
        widthTranslate = lerp (widthTranslate, 0f, 0.5f);
      heightTranslate = step (heightTranslate, 0f, 11f);
    
    if (widthTranslate <= 0f && owner.posOffset < -owner.maxOffset) {
      owner.posOffset += owner.maxOffset;
    }  
  }
}

class StripeMovementWidthScenario extends StripeScenario {
  void initBehavs () {
    baseStripe.yPos = 0f;  
    baseStripe.breakSize = 0f;
    stripeBehavs.add (new StripeXOffsetBehaviour ().init (owner));
    stripeBehavs.add (new StripeWidthBehaviour ().init (owner));
  }
  
  void onBehave () {
    if (owner.posOffset > owner.maxOffset) {   
        owner.posOffset -= owner.maxOffset;
    }
  }
}

class StripeDeplicationScenario extends StripeScenario {
  void initBehavs () {
  }
  
  void onBehave () {
    owner.posOffset = lerp (owner.posOffset, width / 2, 0.15f);
    baseStripe.myWidth = lerp (baseStripe.myWidth, 2f, 0.05f);
    owner.distModif = lerp (owner.distModif, 0f, 0.15f);
  }
}

abstract class StripeScenario {
  StripeController owner;
  
  ArrayList<StripeBehaviour> stripeBehavs = new ArrayList<StripeBehaviour> ();
  
  StripeScenario onStart (StripeController stripeController) {
    this.owner = stripeController;
    stripeBehavs.clear ();
    initBehavs ();
    
    return this;
  }
  
  void initBehavs () {
  }
  
  void behave () {
    onBehave ();
    
    for (StripeBehaviour b : stripeBehavs) {
      b.behave ();
    }
  }
  
  void onBehave () {
  }
}
