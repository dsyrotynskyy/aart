import processing.core.*; 
import processing.data.*; 
import processing.event.*; 
import processing.opengl.*; 

import java.util.HashMap; 
import java.util.ArrayList; 
import java.io.File; 
import java.io.BufferedReader; 
import java.io.PrintWriter; 
import java.io.InputStream; 
import java.io.OutputStream; 
import java.io.IOException; 

public class Aart1 extends PApplet {

int STRIPE_COUNT = 55;
Stripe baseStripe;
StripeController stripeController;
PImage back;

public void setup() { 
  size(640, 640);
  initStripes ();
}

public void initStripes () {
  baseStripe = new Stripe ();
  baseStripe.myWidth = 18f;
  
  stripeController = new StripeController (baseStripe, STRIPE_COUNT);
}

public void draw() {
  noStroke ();
  background (255);
  stripeController.onUpdate ();
}



class Timer {
  int savedTime;
  int totalTime = 1000;
  
  public Timer () {
    savedTime = millis();
  }
  
  public Timer (int totalTime) {
    this ();
    this.totalTime = totalTime;
  }

  public boolean isTime () {
    int passedTime = millis() - savedTime;
    if (passedTime > totalTime) {
      savedTime = millis(); // Save the current time to restart the timer!
      return true;
    }
    return false;
  }
  
  public void forceTime () {
    savedTime -= totalTime;
  }
  
  public void restart () {
    savedTime = millis();
  }
}

public float step (float v, float target, float step) {
  if (step < 0f) {
    println ("Bad arg");
  }
  
  if (v < target) {
    v += step;
  } else {
    v -= step;
  }
  if (abs(abs(v) - abs(target)) <= step) {
    v = target;
  }
 
  return v;
}
class Stripe {
  boolean drawInfinite = false;
  float breakSize = 10f;
  float yPos;
  float xPos;
  float myWidth = 8f;
  float myHeight;
  int myColor = color (0); 
  
  Stripe () {
  }
  
  Stripe (Stripe other) {
    clone (other);
  }
  
  public Stripe clone (Stripe other) {
    this.drawInfinite = other.drawInfinite;
    this.breakSize = other.breakSize;
    this.myWidth = other.myWidth;
    this.myColor = other.myColor;
    this.myHeight = other.myHeight;
    this.yPos = other.yPos;
    return this;
  }
  
  public void lerpWidth (float newWidth) {
    myWidth = newWidth;
  }
  
  public void onDrawByStart () {    
    if (drawInfinite) {
    }
    
    fill (myColor);    
    int totalNum = 10;
    for (int i = 0; i < totalNum; i++) {
      rect(xPos, yPos + (myHeight
      + breakSize
      ) * i, myWidth, myHeight);
      
      if (drawInfinite) {
         rect(xPos, yPos - (myHeight
        + breakSize
        ) * i, myWidth, myHeight);
      }
    }
  }  
}




  abstract class StripeBehaviour {  
    Stripe baseStripe;
    StripeController owner;
    public StripeBehaviour init (StripeController owner) {
      this.owner = owner;
      this.baseStripe = owner.base;
      return this;
    }
    public abstract void behave ();
  }
  
  class StripeDistanceModifBehaviour extends StripeBehaviour {
    boolean incModif;
    
    public void behave () {
      float modifStep = 0.001f;
      
      if (incModif) {
        owner.distModif += modifStep;      
        if (owner.distModif >= 1.3f) {
          incModif = false;
        }
      } else {
        owner.distModif -= modifStep;    
        if (owner.distModif <= 0.7f) {
          incModif = true;
        }
      }
    }
  }
  
  class StripeXOffsetBehaviour extends StripeBehaviour {
    boolean incPosOffset = true;  
  
    public void behave () {
      float modifStep = 0.51f;  
      
//      if (incPosOffset) {
//       owner.posOffset += modifStep;      
//        if (owner.posOffset >= width / 2 + width / 8) {
//          incPosOffset = false;
//        }
//      } else
      {
        owner.posOffset -= modifStep;    
        if (owner.posOffset <= width / 2 - width / 8) {
          incPosOffset = true;
        }
      }
    }
  }
  
  class StripeWidthBehaviour extends StripeBehaviour {
    boolean incWidth = true;  
    float desWidth;
    Timer pauseTimer = new Timer (500);
    float offsetLoop;
    
    public StripeBehaviour init (StripeController owner) {
      super.init (owner);
      desWidth = owner.base.myWidth;
      pauseTimer.forceTime ();  
      return this;
    }
   
    public void behave () {
      if (!pauseTimer.isTime ()) {
            if (desWidth == 0) {
              baseStripe.myWidth = 0f;
            } else {
              pauseTimer.forceTime ();
            }
        return;
      }
     
      if (incWidth) {
        desWidth = owner.maxOffset * 1.1f;  
        
        if (owner.base.myWidth >= owner.maxOffset) {
          incWidth = false;
          pauseTimer.restart ();
        } else {
          pauseTimer.forceTime ();
        }
      } 
      else
      {
        desWidth = 0;
        if (owner.base.myWidth <= 0.0f) {
          incWidth = true;
          pauseTimer.restart ();
        } else {
          pauseTimer.forceTime ();
        }
      }
      
      calcCurWidth ();
    }
    
    public void calcCurWidth () {   
      float inc = 0.5f;
      
      if (baseStripe.myWidth < desWidth) {
        baseStripe.myWidth += inc;
      }
      
      if (baseStripe.myWidth > desWidth) {
        baseStripe.myWidth -= inc;
      }
    }
  }
Timer switchTimer = new Timer (300);

public void mousePressed() {
  if (switchTimer.isTime()) {
    stripeController.switchScenario ();
  }  
}

class StripeController {
  Stripe[] stripes;    
  float maxOffset;
  float distModif = 1f;
  float posOffset;
  Stripe base;
  int stripeIgnorePos = 0;
  
  StripeController (Stripe base, int count) {
    this.base = base;
    distModif = 0f;
    baseStripe.myWidth = 0f;
    this.stripes = new Stripe[count];
    for (int i = 0; i < stripes.length; i++) {
      stripes[i] = new Stripe (base);
    }
    posOffset = width / 2;
    maxOffset = width / Math.max (1, stripes.length) * 4.5f; 
    
    prepareScenarios ();
  }
  
  public void onUpdate () {
    currentScenario.behave ();
    translate(posOffset, 0);  
    
    for (int i = 0; i < stripes.length; i++) {
      if (i < stripeIgnorePos) {
        continue;
      }
      float curX = calcX(i, stripes.length / 2);
      stripes[i].xPos = curX;
      stripes[i].clone(base);
      stripes[i].onDrawByStart ();
    }
  }
  
  public float calcX (int i, int central) {  
    return  maxOffset * (i - central) * distModif;
  }
  
  ArrayList<StripeScenario> stripeScenarios = new ArrayList<StripeScenario> ();
  int currentScenarioNum;
  StripeScenario currentScenario;
   
  public void switchScenario () {
    for (int i = 0; i < stripeScenarios.size(); i++) {
      if (stripeScenarios.get (i) == currentScenario) {
        int nextScenarioPosition = i + 1;
        if (nextScenarioPosition >= stripeScenarios.size()) {
          nextScenarioPosition = 0;
        } 
        currentScenario = stripeScenarios.get (nextScenarioPosition).onStart (this);
        break;
      }
    }
  }
  
  public void prepareScenarios () {
    stripeScenarios.add( new VoidScenario ());
    stripeScenarios.add( new StripeCreationScenario ());
    stripeScenarios.add( new StripeMultiplicationScenario ());
    stripeScenarios.add( new StripeMovementScenario ());
    stripeScenarios.add( new StripeMovementWidthScenario ());
    stripeScenarios.add( new StripeDeplicationScenario ());

    currentScenario = stripeScenarios.get (0).onStart (this);
  }
}

class VoidScenario extends StripeScenario {
  public void onBehave () {
    owner.distModif = lerp (owner.distModif, 0f, 0.15f);
    baseStripe.myWidth = lerp (baseStripe.myWidth, 0f, 0.4f);
    baseStripe.myHeight = height;
    baseStripe.yPos = lerp (baseStripe.yPos, -height, 0.1f);
  }
}

class StripeCreationScenario extends StripeScenario {  
  public void initBehavs () {
    baseStripe.drawInfinite = false;
    owner.posOffset = width / 2;
    baseStripe.yPos = height;
    baseStripe.breakSize = 10f;
    owner.stripeIgnorePos = owner.stripes.length / 2;
  }
  
  public void onBehave () {
    owner.posOffset = 0f;
    owner.distModif = lerp (owner.distModif, 0f, 0.15f);
    
    baseStripe.myWidth = step (baseStripe.myWidth, 2f, 0.5f);
    baseStripe.yPos -= 2.5f;
   
    if (baseStripe.yPos < -height * 3) {
      baseStripe.yPos = -height * 3;
    }   
 
    owner.posOffset = lerp (owner.posOffset, width / 2, 0.15f);
    translate(width, height / 2.5f);  
    rotate (radians (90));   
  }
}

class StripeMultiplicationScenario extends StripeScenario {
  public void onBehave () {
    baseStripe.breakSize = 0f;
    baseStripe.yPos = 0f;
    baseStripe.breakSize = 0f;
    owner.distModif = lerp (owner.distModif, 1f, 0.015f);
    baseStripe.myWidth = lerp (baseStripe.myWidth, 2f, 0.05f);
     
    translate(width, height / 2.5f);  
    rotate (radians (90)); 
  }
}

class StripeMovementScenario extends StripeScenario {
  float rotationDesValue;
  float rotationStep = 0.5f;
  float widthTranslate;
  float heightTranslate;
  
  public void initBehavs () {
     baseStripe.drawInfinite = true;
     rotationDesValue = 90;
     widthTranslate = width;
     heightTranslate = height / 2.5f;
     
     stripeBehavs.add (new StripeXOffsetBehaviour ().init (owner));
  }
  
  public void onBehave () {
    owner.distModif = 1f;
    baseStripe.myWidth = 2f;
    baseStripe.breakSize = 0f;
    baseStripe.yPos = 0f;
    
    translate(widthTranslate, heightTranslate);  
    rotate (radians (rotationDesValue));
    
    rotationDesValue = step (rotationDesValue, 0f, rotationStep);
        widthTranslate = lerp (widthTranslate, 0f, 0.5f);
      heightTranslate = step (heightTranslate, 0f, 11f);
    
    if (widthTranslate <= 0f && owner.posOffset < -owner.maxOffset) {
      owner.posOffset += owner.maxOffset;
    }  
  }
}

class StripeMovementWidthScenario extends StripeScenario {
  public void initBehavs () {
    baseStripe.yPos = 0f;  
    baseStripe.breakSize = 0f;
    stripeBehavs.add (new StripeXOffsetBehaviour ().init (owner));
    stripeBehavs.add (new StripeWidthBehaviour ().init (owner));
  }
  
  public void onBehave () {
    if (owner.posOffset > owner.maxOffset) {   
        owner.posOffset -= owner.maxOffset;
    }
  }
}

class StripeDeplicationScenario extends StripeScenario {
  public void initBehavs () {
  }
  
  public void onBehave () {
    owner.posOffset = lerp (owner.posOffset, width / 2, 0.15f);
    baseStripe.myWidth = lerp (baseStripe.myWidth, 2f, 0.05f);
    owner.distModif = lerp (owner.distModif, 0f, 0.15f);
  }
}

abstract class StripeScenario {
  StripeController owner;
  
  ArrayList<StripeBehaviour> stripeBehavs = new ArrayList<StripeBehaviour> ();
  
  public StripeScenario onStart (StripeController stripeController) {
    this.owner = stripeController;
    stripeBehavs.clear ();
    initBehavs ();
    
    return this;
  }
  
  public void initBehavs () {
  }
  
  public void behave () {
    onBehave ();
    
    for (StripeBehaviour b : stripeBehavs) {
      b.behave ();
    }
  }
  
  public void onBehave () {
  }
}
  static public void main(String[] passedArgs) {
    String[] appletArgs = new String[] { "Aart1" };
    if (passedArgs != null) {
      PApplet.main(concat(appletArgs, passedArgs));
    } else {
      PApplet.main(appletArgs);
    }
  }
}
