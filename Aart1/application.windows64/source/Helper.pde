class Timer {
  int savedTime;
  int totalTime = 1000;
  
  public Timer () {
    savedTime = millis();
  }
  
  public Timer (int totalTime) {
    this ();
    this.totalTime = totalTime;
  }

  public boolean isTime () {
    int passedTime = millis() - savedTime;
    if (passedTime > totalTime) {
      savedTime = millis(); // Save the current time to restart the timer!
      return true;
    }
    return false;
  }
  
  void forceTime () {
    savedTime -= totalTime;
  }
  
  void restart () {
    savedTime = millis();
  }
}

float step (float v, float target, float step) {
  if (step < 0f) {
    println ("Bad arg");
  }
  
  if (v < target) {
    v += step;
  } else {
    v -= step;
  }
  if (abs(abs(v) - abs(target)) <= step) {
    v = target;
  }
 
  return v;
}
