  abstract class StripeBehaviour {  
    Stripe baseStripe;
    StripeController owner;
    StripeBehaviour init (StripeController owner) {
      this.owner = owner;
      this.baseStripe = owner.base;
      return this;
    }
    abstract void behave ();
  }
  
  class StripeDistanceModifBehaviour extends StripeBehaviour {
    boolean incModif;
    
    void behave () {
      float modifStep = 0.001f;
      
      if (incModif) {
        owner.distModif += modifStep;      
        if (owner.distModif >= 1.3f) {
          incModif = false;
        }
      } else {
        owner.distModif -= modifStep;    
        if (owner.distModif <= 0.7f) {
          incModif = true;
        }
      }
    }
  }
  
  class StripeXOffsetBehaviour extends StripeBehaviour {
    boolean incPosOffset = true;  
  
    void behave () {
      float modifStep = 0.51f;  
      
//      if (incPosOffset) {
//       owner.posOffset += modifStep;      
//        if (owner.posOffset >= width / 2 + width / 8) {
//          incPosOffset = false;
//        }
//      } else
      {
        owner.posOffset -= modifStep;    
        if (owner.posOffset <= width / 2 - width / 8) {
          incPosOffset = true;
        }
      }
    }
  }
  
  class StripeWidthBehaviour extends StripeBehaviour {
    boolean incWidth = true;  
    float desWidth;
    Timer pauseTimer = new Timer (500);
    float offsetLoop;
    
    StripeBehaviour init (StripeController owner) {
      super.init (owner);
      desWidth = owner.base.myWidth;
      pauseTimer.forceTime ();  
      return this;
    }
   
    void behave () {
      if (!pauseTimer.isTime ()) {
            if (desWidth == 0) {
              baseStripe.myWidth = 0f;
            } else {
              pauseTimer.forceTime ();
            }
        return;
      }
     
      if (incWidth) {
        desWidth = owner.maxOffset * 1.1f;  
        
        if (owner.base.myWidth >= owner.maxOffset) {
          incWidth = false;
          pauseTimer.restart ();
        } else {
          pauseTimer.forceTime ();
        }
      } 
      else
      {
        desWidth = 0;
        if (owner.base.myWidth <= 0.0f) {
          incWidth = true;
          pauseTimer.restart ();
        } else {
          pauseTimer.forceTime ();
        }
      }
      
      calcCurWidth ();
    }
    
    void calcCurWidth () {   
      float inc = 0.5f;
      
      if (baseStripe.myWidth < desWidth) {
        baseStripe.myWidth += inc;
      }
      
      if (baseStripe.myWidth > desWidth) {
        baseStripe.myWidth -= inc;
      }
    }
  }
