class Stripe {
  boolean drawInfinite = false;
  float breakSize = 10f;
  float yPos;
  float xPos;
  float myWidth = 8f;
  float myHeight;
  color myColor = color (0); 
  
  Stripe () {
  }
  
  Stripe (Stripe other) {
    clone (other);
  }
  
  Stripe clone (Stripe other) {
    this.drawInfinite = other.drawInfinite;
    this.breakSize = other.breakSize;
    this.myWidth = other.myWidth;
    this.myColor = other.myColor;
    this.myHeight = other.myHeight;
    this.yPos = other.yPos;
    return this;
  }
  
  void lerpWidth (float newWidth) {
    myWidth = newWidth;
  }
  
  void onDrawByStart () {    
    if (drawInfinite) {
    }
    
    fill (myColor);    
    int totalNum = 10;
    for (int i = 0; i < totalNum; i++) {
      rect(xPos, yPos + (myHeight
      + breakSize
      ) * i, myWidth, myHeight);
      
      if (drawInfinite) {
         rect(xPos, yPos - (myHeight
        + breakSize
        ) * i, myWidth, myHeight);
      }
    }
  }  
}




