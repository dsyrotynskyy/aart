  abstract class HoriStripeBeh {
    StripeController owner;
    
    HoriStripeBeh init (StripeController owner) {
      this.owner = owner;
      return this;
    }
    abstract void onBehave ();
  }

  class HoriStripeBehCreation extends HoriStripeBeh {
    Timer t = new Timer(100);
    int horiStripesDesCount = 0;
    
    @Override
    public void onBehave() {
    }    
  }

  class HoriStripeBehStop extends HoriStripeBeh {
    @Override
    public void onBehave() {
    }    
  }

  class HoriStripeBehFalling extends HoriStripeBeh {
    @Override
    public void onBehave() {
      for (int i = 0; i < owner.horiStripes.size(); i++) {
        HoriStripe h = owner.horiStripes.get(i);
        if (owner.owner.random(100) > 90)
          h.yStep = owner.owner.random(1000) / 200;
        h.update();

        if (owner.owner.random(100) > 90) {
          h.targetRotation = 0;
        }
      }
    }    
  }

  class HoriStripeBehRotation extends HoriStripeBeh {
    @Override
    public void onBehave() {
      for (int i = 0; i < owner.horiStripes.size(); i++) {
        HoriStripe h = owner.horiStripes.get(i);

        if (owner.owner.random(100) > 90) {
          h.targetRotation = owner.owner.random(360 * 2) - 360;
        }

        if (owner.owner.random(100) > 90) {
          h.targetRotation = 0;
        }

        h.update();
      }
    }    
  }

  class HoriStripeBehRandomizingMovement extends HoriStripeBeh {
    @Override
    public void onBehave() {
      for (int i = 0; i < owner.horiStripes.size(); i++) {
        HoriStripe h = owner.horiStripes.get(i);
        if (owner.owner.random(100) > 90) {
          h.yStep = owner.owner.random(1000) / 100 - 5;
          h.xStep = owner.owner. random(1000) / 100 - 5;
        }

        h.update();
      }    
    }    
  }
  
  abstract class BaseHoriScenario extends StripeMovementWidthScenario {
    HashMap<HoriStripeBeh, Boolean> horiStripeBehs = new HashMap<HoriStripeBeh, Boolean>  ();
    
    @Override
    void initBehavs() {
      super.initBehavs();
    }

    @Override
    void onBehave() {
      super.onBehave();

      for (HoriStripeBeh beh : horiStripeBehs.keySet()) {
        if (horiStripeBehs.get(beh)) {
          beh.onBehave();
        }
      }
    }
  }
  
    class HoriStripe {
    float xPos;
    float yPos = 1f;
    float myWidth = 5f;
    float myHeight = 1f;
    float targetRotation = 0f;

    StripeController owner;

    Timer blinkTimer;
    Timer disTimer;

    boolean dissapear = false;

    public HoriStripe(StripeController owner) {
      this.owner = owner;

      yPos = random(width);

      xPos += owner.widthOffset;
      yPos += owner.heightOffset;

      if (random(100) > 80) {
        this.blinkTimer = new Timer((int) random(250));
      }

      if (random(100) > 80) {
        this.disTimer = new Timer((int) random(250));
      }
    }

    public HoriStripe(StripeController owner, Stripe stripe) {
      this(owner);

      this.xPos += stripe.xPos;
      this.myWidth = stripe.myWidth;
      if (myWidth > 0)
        myWidth = owner.maxOffset;

      this.xPos -= stripe.myWidth / 5f;
    }

    public void onDraw() {
      fill(255);

      if (this.blinkTimer != null && this.blinkTimer.isTime()) {
        return;
      }

      if (this.disTimer != null && this.disTimer.isTime()) {
        dissapear = !dissapear;
      }

      pushMatrix();
      float x = xPos - owner.widthOffset;
      float y = yPos - owner.heightOffset;
      translate(x, y);
      rotate(radians(curRotation));

      if (!dissapear) {
        rectMode(CENTER);
        rect(0, 0, myWidth, myHeight);
        rectMode(CORNER);
      }

      popMatrix();
    }

    float xStep;
    float yStep;
    float curRotation;

    void update() {
      this.xPos += xStep;
      this.yPos += yStep;

      if (this.xPos > height) {
        this.xPos -= height;
        this.yPos = random(width);
      }

      if (this.yPos > width) {
        this.yPos -= width;
        this.xPos = random(height);
      }

      curRotation = lerp(curRotation, this.targetRotation, 10 * deltaTime);
    }
  }


