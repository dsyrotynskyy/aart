
 
class AudioController {
    
}

float MAX_VOLUME = 0f;
float NO_VOLUME = -40f;


void playSong (AudioPlayer _song) {
  _song.shiftGain (MAX_VOLUME, MAX_VOLUME, 1000);
}

void stopSong (AudioPlayer _song) {
  _song.shiftGain (MAX_VOLUME, NO_VOLUME, 1000);
}
