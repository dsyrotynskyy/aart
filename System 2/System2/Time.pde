  class Time
  {

    private long currentTime;
    private long previousTime;
    private double deltaTime;
    private double countTime;  
    private float timeScale = 1.0f;

    PApplet parent;

    @SuppressWarnings("deprecation")
    public Time(PApplet _parent)
    {

      parent = _parent;

      parent.registerPre(this);

      previousTime = currentTime;

      currentTime = millis();
      
      deltaTime = 0;
      
      countTime = 0;
    }

    /**
     * Called by the PApplet before draw(). 
     * NOTE: This method is automatic and should not be called by the user. 
     */
    public void pre()
    {

      previousTime = currentTime;

      currentTime = millis(); 
      
      // remember kids: time is counted as a long with 1000 to a second (millisecond)
      // hense the 0.001f to get the float value of deltaTime.
      deltaTime = (currentTime - previousTime) * 0.001d * timeScale;
      
      countTime += deltaTime;

    }
    
    public float getDeltaTime() 
    {
      return (float)deltaTime;
    }

    public float getTimeScale() {
      return timeScale;
    }

    public void setTimeScale(float timeScale) {
      this.timeScale = timeScale;
    }

    public float getCountTime() {
      return (float)countTime;
    }
    
    public void resetTime(){
      
    }
  }
  
    class Timer {
    public int savedTime;
    public int totalTime = 1000;

    public Timer() {
      savedTime = (int) millis();
    }

    public Timer(int totalTime) {
      this();
      this.totalTime = totalTime;
    }

    public boolean isTime() {
      int passedTime = (int) millis() - savedTime;
      if (passedTime > totalTime) {
        savedTime = (int) millis(); // Save the current time to restart the
                    // timer!
        return true;
      }
      return false;
    }

    void forceTime() {
      savedTime -= totalTime;
    }

    void restart() {
      savedTime = (int) millis();
    }
  }
