  import ddf.minim.*;
  
  float STRIPE_WIDTH_SPEED = 100f;
  int STRIPE_COUNT = 75;
  Stripe baseStripe;
  StripeController stripeController;
  AudioController audioController;
  PImage back;

  public void setup() {
    // size (1280, 1024);
//   size(640, 640);
    float v = 1.5f;
    if (true) {
      size((int)(1000 / v),(int)( 539 / v));
    } else {
      size((int)(539 / v),(int)( 1000 / v));
    }
    
    initStripes();

    currentTime = millis();
    previousTime = currentTime;
    deltaTime = 0;
    
//    frameRate (120);
    this.setupSound ();
  }

  void initStripes() {
    baseStripe = new Stripe();
    baseStripe.myWidth = 18f;

    stripeController = new StripeController(baseStripe, this, STRIPE_COUNT);
    audioController = new AudioController();
  }

  private static long currentTime;
  private static long previousTime;
  private static float deltaTime = -1;

  public void draw() {
//    rotate (radians(-90));
    
    calculateTimeScale();

    noStroke();
    background(255);

    pushMatrix();
    stripeController.onUpdate();
    popMatrix();
    this.noCursor();
    
    // this.calculateFPS();
  }

  int fcount, lastm;
  float frate;
  int fint = 3;

  void calculateFPS() {
    fcount += 1;
    int m = millis();
    if (m - lastm > 1000 * fint) {
      frate = (float) (fcount) / fint;
      fcount = 0;
      lastm = m;
      println("fps: " + frate);
    }
    fill(0);
    text("fps: " + frate, 10, 20);
  }

  void calculateTimeScale() {
    previousTime = currentTime;
    currentTime = millis();

    float timeOffset = (currentTime - previousTime) * 0.001f;
    if (deltaTime > 0f) {
      deltaTime = lerp(deltaTime, timeOffset, 0.1f);
    } else {
      deltaTime = timeOffset;
    }
    // println ("delta time " + deltaTime);
  }

  float step(float v, float target, float step) {
    if (step < 0f) {
      // println("Bad arg");
    }

    if (v < target) {
      v += step;
    } else {
      v -= step;
    }
    if (abs(abs(v) - abs(target)) <= step) {
      v = target;
    }

    return v;
  }

  class Stripe {
    public boolean drawInfinite = false;
    public float breakSize = 10f;
    public float yPos;
    public float xPos;
    public float myWidth = 8f;
    public float myHeight;

    Stripe() {
    }

    Stripe(Stripe other) {
      clone(other);
    }

    Stripe clone(Stripe other) {
      this.drawInfinite = other.drawInfinite;
      this.breakSize = other.breakSize;
      this.myWidth = other.myWidth;
      this.myHeight = other.myHeight;
      this.yPos = other.yPos;
      return this;
    }

    void onDrawByStart() {
      fill(0);
      int totalNum = 5;
      breakSize = 0f;
      for (int i = 0; i < totalNum; i++) {
        rect(xPos, yPos - (myHeight + breakSize) * i, myWidth, myHeight);

        if (drawInfinite) {
          rect(xPos, yPos + (myHeight + breakSize) * i, myWidth,
              myHeight);
        }
      }
    }
  }

  class HoriStripe {
    float xPos;
    float yPos = 1f;
    float myWidth = 5f;
    float myHeight = 1f;
    float targetRotation = 0f;

    StripeController owner;

    Timer blinkTimer;
    Timer disTimer;

    boolean dissapear = false;

    public HoriStripe(StripeController owner) {
      this.owner = owner;

      yPos = random(height);

      xPos += owner.widthOffset;
      yPos += owner.heightOffset;

      if (random(100) > 80) {
        this.blinkTimer = new Timer((int) random(250));
      }

      if (random(100) > 80) {
        this.disTimer = new Timer((int) random(250));
      }
    }

    public HoriStripe(StripeController owner, Stripe stripe) {
      this(owner);

      this.xPos += stripe.xPos;
      this.myWidth = stripe.myWidth;
      if (myWidth > 0)
        myWidth = owner.maxOffset;

      this.xPos -= stripe.myWidth / 5f;
    }

    public void onDraw() {
      fill(255);

      if (this.blinkTimer != null && this.blinkTimer.isTime()) {
        return;
      }

      if (this.disTimer != null && this.disTimer.isTime()) {
        dissapear = !dissapear;
      }

      pushMatrix();
      float x = xPos - owner.widthOffset;
      float y = yPos - owner.heightOffset;
      translate(x, y);
      rotate(radians(curRotation));

      if (!dissapear) {
        rectMode(CENTER);
        rect(0, 0, myWidth, myHeight);
        rectMode(CORNER);
      }

      popMatrix();
    }

    float xStep;
    float yStep;
    float curRotation;

    void update() {
      this.xPos += xStep;
      this.yPos += yStep;

      if (this.xPos > width) {
        this.xPos -= width;
        this.yPos = random(height);
      }

      if (this.yPos > height) {
        this.yPos -= height;
        this.xPos = random(width);
      }

      curRotation = lerp(curRotation, this.targetRotation, 10 * deltaTime);
    }
  }

  abstract class StripeBehaviour {
    Stripe baseStripe;
    StripeController owner;

    StripeBehaviour init(StripeController owner) {
      this.owner = owner;
      this.baseStripe = owner.base;
      return this;
    }

    abstract void behave();
  }

  class StripeWidthBehaviour extends StripeBehaviour {
    boolean incWidth = true;
    float desWidth;
    Timer pauseTimer = new Timer(0);
    float offsetLoop;

    StripeBehaviour init(StripeController owner) {
      super.init(owner);
      desWidth = owner.base.myWidth;
      pauseTimer.forceTime();
      return this;
    }

    void behave() {
      if (!pauseTimer.isTime()) {
        if (desWidth == 0) {
          baseStripe.myWidth = 0f;
        } else {
          pauseTimer.forceTime();
        }
        return;
      }

      if (incWidth) {
        desWidth = owner.maxOffset * 1.1f;

        if (owner.base.myWidth >= owner.maxOffset) {
          incWidth = false;
          pauseTimer.restart();
        } else {
          pauseTimer.forceTime();
        }
      } else {
        desWidth = 0;
        if (owner.base.myWidth <= 0.0f) {
          incWidth = true;
          pauseTimer.restart();
        } else {
          pauseTimer.forceTime();
        }
      }

      calcCurWidth();
    }

    void calcCurWidth() {
      float inc = (float) (STRIPE_WIDTH_SPEED / owner.maxOffset * deltaTime);

      if (baseStripe.myWidth < desWidth) {
        baseStripe.myWidth += inc;
        // owner.widthOffset -= WIDTH_SPEED * deltaTime / 2f;
      }

      if (baseStripe.myWidth > desWidth) {
        baseStripe.myWidth -= inc * 2f;

        owner.widthOffset += inc * 2f;
        // owner.widthOffset -= WIDTH_SPEED * deltaTime / 2f;
      }
    }
  }
  Timer mouseTimer = new Timer(300);

  public void mousePressed() {
    if (mouseTimer.isTime()) {

      if (mouseButton == LEFT) {
        stripeController.switchScenario();
      } else {
        stripeController.switchScenarioBack();
      }
    }
  }

  public void keyPressed() {
    if (key == CODED) {
      if (keyCode == LEFT) {
        stripeController.switchScenarioBack();
      }
      if (keyCode == RIGHT) {
        stripeController.switchScenario();
      }
    }
  }

  class StripeController {
    ArrayList<HoriStripe> horiStripes = new ArrayList<HoriStripe>(50);

    Stripe[] stripes;
    float maxOffset;
    float distModif = 1f;

    Stripe base;
    int stripeIgnorePos = 0;

    float widthOffset;
    float heightOffset;
    float rotationDesValue;
    
    PApplet owner;

    StripeController(Stripe base, PApplet owner, int count) {
      this.base = base;
      this.owner = owner;
      distModif = 0f;
      baseStripe.myWidth = 0f;
      this.stripes = new Stripe[count];
      for (int i = 0; i < stripes.length; i++) {
        stripes[i] = new Stripe(base);
      }
      maxOffset = width / Math.max(1, stripes.length) * 4.5f;

      prepareScenarios();
    }

    float calcX(int i, int central) {
      return maxOffset * (i - central) * distModif;
    }

    ArrayList<StripeScenario> stripeScenarios = new ArrayList<StripeScenario>();
    int currentScenarioNum;
    StripeScenario currentScenario;

    void switchScenario() {
      for (int i = 0; i < stripeScenarios.size(); i++) {
        if (stripeScenarios.get(i) == currentScenario) {
          int nextScenarioPosition = i + 1;
          if (nextScenarioPosition >= stripeScenarios.size()) {
            nextScenarioPosition = 0;
          }
          currentScenario.onExit ();
          currentScenario = stripeScenarios.get(nextScenarioPosition)
              .onStart(this);
          break;
        }
      }
    }

    void switchScenarioBack() {
      for (int i = 0; i < stripeScenarios.size(); i++) {
        if (stripeScenarios.get(i) == currentScenario) {
          int nextScenarioPosition = i - 1;
          if (nextScenarioPosition < 0) {
            nextScenarioPosition = stripeScenarios.size() - 1;
          }
          
          currentScenario.onExit ();
          currentScenario = stripeScenarios.get(nextScenarioPosition)
              .onStart(this);
          break;
        }
      }
    }

    void onUpdate() {
      currentScenario.behave();
      rotate(radians(rotationDesValue));
      translate(widthOffset, heightOffset);

      base.onDrawByStart();

      for (int i = 0; i < stripes.length; i++) {
        if (i > stripeIgnorePos) {
          continue;
        }
        float curX = calcX(i, stripes.length / 2);
        stripes[i].xPos = curX;
        stripes[i].clone(base);
        stripes[i].onDrawByStart();
      }

      for (int i = 0; i < horiStripes.size(); i++) {
        HoriStripe s = this.horiStripes.get(i);
        if (s != null)
          s.onDraw();
      }
    }

    void prepareScenarios() {
//      stripeScenarios.add(new VoidScenario());

      stripeScenarios.add(new StripeCreationScenario());
      stripeScenarios.add(new StripeMultiplicationScenario());
      stripeScenarios.add(new StripeMovementScenario());
      stripeScenarios.add(new StripeMovementWidthActiveScenario());

      stripeScenarios.add(new Hori1Scenario_Creation());
      stripeScenarios.add(new Hori2Scenario_StopMotion());
      stripeScenarios.add(new Hori3Scenario_Falling());
      stripeScenarios.add(new Hori4Scenario_FallingRotation());
      stripeScenarios.add(new Hori5Scenario_Randomizing());
      stripeScenarios.add(new SuperHoriScenario());

      stripeScenarios.add(new StripeDeplicationScenario());

      currentScenario = stripeScenarios.get(0).onStart(this);
    }
  }


  



