abstract class StripeScenario {
  StripeController owner;

  ArrayList<StripeBehaviour> stripeBehavs = new ArrayList<StripeBehaviour>();

  StripeScenario onStart(StripeController stripeController) {
    this.owner = stripeController;
    stripeBehavs.clear();
    initBehavs();

    return this;
  }

  void initBehavs() {
  }

  void behave() {
    onBehave();

    for (StripeBehaviour b : stripeBehavs) {
      b.behave();
    }
  }

  void onBehave() {
  }

  void onExit () {
  }
}

class VoidScenario extends StripeScenario {
  Timer t = new Timer (4000);

  void onBehave() {
    owner.horiStripes.clear();

    owner.distModif = lerp(owner.distModif, 0f, 0.15f);
    baseStripe.myWidth = lerp(baseStripe.myWidth, 0f, 0.4f);
    baseStripe.myHeight = height;
    baseStripe.yPos = lerp(baseStripe.yPos, -height, 0.1f);
  }
}

boolean isRotated = false;

class StripeCreationScenario extends StripeScenario {
  AudioPlayer song;
  {
    song = minim.loadFile("scene1.wav", 1048);
  }

  void initBehavs() {
    baseStripe.drawInfinite = false;
    baseStripe.yPos = 0f;
    baseStripe.breakSize = 10f;
    owner.stripeIgnorePos = 0;

    owner.heightOffset = 0f;
    owner.widthOffset = -height * 0.5f;
    baseStripe.myHeight = width;

    if (isRotated) {
      owner.rotationDesValue = 0f;
    } else
      owner.rotationDesValue = -90f;

    song.play ();
    song.shiftGain (MAX_VOLUME, MAX_VOLUME, 300);
  }

  void onExit () {
    song.shiftGain (MAX_VOLUME, NO_VOLUME, 1000);
  }

  void onBehave() {
    owner.distModif = lerp(owner.distModif, 0f, 0.15f);

    baseStripe.myWidth = step(baseStripe.myWidth, 2f, 0.5f);
    baseStripe.yPos += 551.5f * deltaTime;

    if (baseStripe.yPos > width) {
      baseStripe.yPos -= width;
    }
  }
}

class StripeMultiplicationScenario extends StripeScenario {
  AudioPlayer song;
  {
    song = minim.loadFile("scene2.wav", 1048);
  }

  void initBehavs() {
    song.play ();
    song.shiftGain (MAX_VOLUME, MAX_VOLUME, 300);
  } 

  void onExit () {
    song.shiftGain (MAX_VOLUME, NO_VOLUME, 1000);
  }

  void onBehave() {
    owner.stripeIgnorePos = owner.stripes.length / 2;
    baseStripe.drawInfinite = true;
    baseStripe.breakSize = 0f;
    baseStripe.yPos = 0f;
    baseStripe.breakSize = 0f;
    owner.distModif = lerp(owner.distModif, 1f, 0.015f);
  }
}

Minim minim = new Minim (this); 

AudioPlayer song3;
AudioPlayer song51;
AudioPlayer song52;
AudioPlayer song53;
AudioPlayer song54;

void setupSound (){
  song3 = minim.loadFile("scene3.wav", 1048);
  song51 = minim.loadFile("scene5,1.wav", 1048);
  song52 = minim.loadFile("scene5,2 on 1,3.wav", 1048);
  song53 = minim.loadFile("scene5,3.wav", 1048);
  song54 = minim.loadFile("scene5,3.wav", 1048);
}

float rotationStep = 2f;

class StripeMovementScenario extends StripeScenario {

  void initBehavs() {
    baseStripe.drawInfinite = true;
    song3.play ();
  }

  void onBehave() {
    owner.distModif = 1f;
    baseStripe.myWidth = 2f;
    baseStripe.breakSize = 0f;
    baseStripe.yPos = 0f;

    if (isRotated) {
      owner.rotationDesValue = step(owner.rotationDesValue, -90f, 
      rotationStep);
    } else
      owner.rotationDesValue = step(owner.rotationDesValue, 0f, 
      rotationStep);

    if (owner.rotationDesValue != 0f) {
      owner.widthOffset += 20 * WIDTH_SPEED * deltaTime;
    } else {
      owner.widthOffset += widthOffsetMovementDelta();
    }

    if (owner.widthOffset > width) {
      owner.widthOffset -= owner.maxOffset;
    }
  }
}

float WIDTH_SPEED = 50f;
StripeWidthBehaviour widthBeh;

abstract class StripeMovementWidthScenario extends StripeScenario {

  void initBehavs() {
    if (isRotated) {
      owner.rotationDesValue = -90f;
    } else
      owner.rotationDesValue = 0f;
    baseStripe.yPos = 0f;
    baseStripe.breakSize = 0f;

    if (widthBeh == null) {
      widthBeh = new StripeWidthBehaviour();
    }

    prepareWidthBehaviour();
  }

  void prepareWidthBehaviour() {
    stripeBehavs.add(widthBeh.init(owner));
  }

  void onBehave() {
    if (owner.widthOffset > width) {
      owner.widthOffset -= owner.maxOffset;
    }

    owner.widthOffset += widthOffsetMovementDelta();

    if (owner.widthOffset < width) {
      owner.widthOffset += owner.maxOffset;
    }
  }
}

class StripeMovementWidthActiveScenario extends StripeMovementWidthScenario {
}

int horiStripesDesCount;

class Hori1Scenario_Creation extends StripeMovementWidthScenario {
  Timer t = new Timer(100);

  @Override
  void initBehavs() {
    song51.play ();
    
    super.initBehavs();
    horiStripesDesCount = 0;
    t.forceTime();
  }

  @Override
    void onBehave() {
    super.onBehave();

    if (t.isTime()) {
      owner.horiStripes.clear();
      horiStripesDesCount += 35;
      for (int i = 0; i < horiStripesDesCount; i++) {
        int rNum = (int) random(owner.stripes.length);
        Stripe randomStripe = owner.stripes[rNum];
        HoriStripe h = new HoriStripe(owner, randomStripe);
        h.yPos -= owner.heightOffset;
        owner.horiStripes.add(h);
      }
    }
  }
}

class Hori2Scenario_StopMotion extends StripeMovementWidthScenario {

  @Override
  void initBehavs() {
    song52.play ();
    
    super.initBehavs();
    for (int i = 0; i < horiStripesDesCount; i++) {
      int rNum = (int) random(owner.stripes.length);
      Stripe randomStripe = owner.stripes[rNum];
      HoriStripe h = new HoriStripe(owner, randomStripe);
      h.yPos -= owner.heightOffset;
      owner.horiStripes.add(h);
    }
  }
}

class Hori3Scenario_Falling extends StripeMovementWidthScenario {

  void initBehavs() {
    song53.play ();
    
    super.initBehavs();
    for (int i = 0; i < owner.horiStripes.size (); i++) {
      HoriStripe h = owner.horiStripes.get(i);
      h.yStep = random(1000) / 200;
    }
  }

  void onBehave() {
    super.onBehave();

    for (int i = 0; i < owner.horiStripes.size (); i++) {
      HoriStripe h = owner.horiStripes.get(i);
      if (random(100) > 90)
        h.yStep = random(1000) / 200;
      h.update();

      if (random(100) > 90) {
        h.targetRotation = 0;
      }
    }
  }
}


class Hori4Scenario_FallingRotation extends StripeMovementWidthScenario {

  @Override
  void initBehavs() {
    song54.play ();
    
    super.initBehavs();

    for (int i = 0; i < horiStripesDesCount; i++) {
      int rNum = (int) random(owner.stripes.length);
      Stripe randomStripe = owner.stripes[rNum];
      HoriStripe h = new HoriStripe(owner, randomStripe);
      h.yPos -= owner.heightOffset;
      owner.horiStripes.add(h);
    }
  }

  void onBehave() {
    super.onBehave();      

    for (int i = 0; i < owner.horiStripes.size (); i++) {
      HoriStripe h = owner.horiStripes.get(i);

      if (random(100) > 90) {
        h.targetRotation = random(360 * 2) - 360;
      }

      if (random(100) > 90) {
        h.targetRotation = 0;
      }

      if (random(100) > 90)
        h.yStep = random(1000) / 200;
      h.update();
    }
  }
}

class Hori5Scenario_Randomizing extends StripeMovementWidthScenario {    
  HoriStripeBehRandomizingMovement beh;

  @Override
    void initBehavs() {
    super.initBehavs();
    beh = (HoriStripeBehRandomizingMovement) new HoriStripeBehRandomizingMovement().init(owner);
  }

  void onBehave() {
    super.onBehave();
    beh.onBehave();
  }
}

float widthOffsetMovementDelta() {
  return WIDTH_SPEED * deltaTime;
}

class SuperHoriScenario extends BaseHoriScenario { 
  Timer t = new Timer ((int)(500 + random (500)));
  Timer mySwitchTimer = new Timer ((int)(1000 * 30));

  @Override
  void initBehavs() {
    print ("Stop song3");
    song3.shiftGain (MAX_VOLUME, NO_VOLUME, 1000);
    song51.shiftGain (MAX_VOLUME, NO_VOLUME, 1000);
    super.initBehavs();

    this.horiStripeBehs.put(new HoriStripeBehStop().init(this.owner), true);
    this.horiStripeBehs.put(new HoriStripeBehFalling().init(this.owner), true);
    this.horiStripeBehs.put(new HoriStripeBehRotation().init(this.owner), true);
    this.horiStripeBehs.put(new HoriStripeBehRandomizingMovement().init(this.owner), true);
  }

  @Override
  void onBehave() {
    super.onBehave();

    if (t.isTime()) {
      t = new Timer ((int)(200 + random (5500)));

      for (HoriStripeBeh beh : horiStripeBehs.keySet ()) {          
        horiStripeBehs.put(beh, false);
      }      

      int pos = (int) random (horiStripeBehs.size());
      int i = 0;
      for (HoriStripeBeh beh : horiStripeBehs.keySet ()) {          
        if (i == pos) {
          horiStripeBehs.put(beh, true);
        }        
        i++;
      }
    }
  }
  
  void onExit () {
    stopSong (song52);
    stopSong (song53);
    stopSong (song54);
    stopSong (song51);
  }
}

class StripeDeplicationScenario extends StripeScenario {
  HashMap<HoriStripeBeh, Boolean> horiStripeBehs = new HashMap<HoriStripeBeh, Boolean>  ();
  
  void initBehavs() {
    owner.horiStripes.clear();
    
    this.horiStripeBehs.put(new HoriStripeBehStop().init(this.owner), true);
    this.horiStripeBehs.put(new HoriStripeBehFalling().init(this.owner), true);
    this.horiStripeBehs.put(new HoriStripeBehRotation().init(this.owner), true);
    this.horiStripeBehs.put(new HoriStripeBehRandomizingMovement().init(this.owner), true);
  }

  void onBehave() {
    for (HoriStripeBeh beh : horiStripeBehs.keySet()) {
        if (horiStripeBehs.get(beh)) {
          beh.onBehave();
        }
      }      
 
    if (isRotated) {
          owner.rotationDesValue = step(owner.rotationDesValue, 0f, rotationStep);

    } else {
          owner.rotationDesValue = step(owner.rotationDesValue, -90f, rotationStep);

    }
    
    if (owner.rotationDesValue != 0f) {
      println ("owner.widthOffset " + owner.widthOffset);
    } 
    
    owner.widthOffset = lerp(owner.widthOffset, -height * 0.5f, 0.15f);
    baseStripe.myWidth = lerp(baseStripe.myWidth, 2f, 0.05f);
    owner.distModif = lerp(owner.distModif, 0f, 0.15f);
    
    owner.distModif = lerp(owner.distModif, 0f, 0.15f);

    baseStripe.myWidth = step(baseStripe.myWidth, 2f, 0.5f);
    baseStripe.yPos += 551.5f * deltaTime;

    if (baseStripe.yPos > height * 3) {
      baseStripe.yPos -= height * 2;
    }
  }
}




