static class Configurator{    
  static class Pixel {
    final static int WIDTH = 7;
    final static int HEIGHT = 7;
    
    final static int MAX_QUANTITY = 1000000;
    final static int START_QUANTITY_MUL = 100;//start quantity = original * START_QUANTITY_MUL   
    final static int SPEED_MUL = 100;//speed = original * SPEED_MUL   
    final static int TRAJECTORY_CHANGE_PERIOD = 1800;
    final static int MATCH_TIMER_PERIOD = 100;     
    
    final static boolean FILL_PIXEL = false;
  }
   
  static class Cell {  
    final static int WIDTH  = 105;
    final static int HEIGHT = 45;    
 
    final static boolean VISIBLE_BORDER       = false;//set true if you want to see borders between cells
    final static boolean REACT_ON_PIXEL_ENTER = false;//set false if you want cells not became red when pixel enters
  }

  static class Field {
    final static int NUM_COLS = 10;
    final static int NUM_ROWS = 10;    
  } 
  
  static class Sound {    
    final static int MAX_SOUNDS = 10;
    final static int PLAYLIST_TIMER_PERIOD = 50;   

    final static boolean AVALAIBLE = true;
  }
}
