import processing.core.*; 
import processing.xml.*; 

import ddf.minim.*; 
import ddf.minim.signals.*; 
import ddf.minim.analysis.*; 
import ddf.minim.effects.*; 
import processing.opengl.*; 
import java.lang.reflect.Method; 

import java.applet.*; 
import java.awt.Dimension; 
import java.awt.Frame; 
import java.awt.event.MouseEvent; 
import java.awt.event.KeyEvent; 
import java.awt.event.FocusEvent; 
import java.awt.Image; 
import java.io.*; 
import java.net.*; 
import java.text.*; 
import java.util.*; 
import java.util.zip.*; 
import java.util.regex.*; 

public class Pythagoras extends PApplet {


class Cell {
  private int x = 0;
  private int y = 0;
  
  private int entered = 0; //for test
  
  Cell(int x, int y) {
    this.x = x;
    this.y = y;
  } 
  
  /**
  * Use function to draw the cell
  */
  public void draw() {
    if (Configurator.Cell.REACT_ON_PIXEL_ENTER && this.entered > 0) {
      this.entered--;
      fill(223, 0, 0);
    } else {
      noFill();
    }
    
    if (!Configurator.Cell.VISIBLE_BORDER) {
      noStroke();
    }
    
    rect(x,
         y,
         Configurator.Cell.WIDTH,
         Configurator.Cell.HEIGHT
    );
  }
  
  /**
  * Call this function when pixel enters cell
  */
  public void onPixelEnter(MovingPixel movingPixel) {
    this.entered = 50;//for test
    
    movingPixel.cell = this;
    soundMaster.addSoundToPlaylist();
  }

  /**
  * Use this function to match a moving pixel in the cell
  */
  public boolean matchPixel(MovingPixel movingPixel) {
    return range(movingPixel.currentX + movingPixel.pixelWidth / 2, x, x + Configurator.Cell.WIDTH) && 
           range(movingPixel.currentY + movingPixel.pixelHeight / 2, y, y + Configurator.Cell.HEIGHT) ? true : false;
  }

  /**
    * Use to check range of victim between val1 nad val2
    */
  public boolean range(int victim, int val1, int val2) {
    return(victim >= val1 && victim < val2);
  }  
}

class CellField {  
  final int cols = Configurator.Field.NUM_COLS;
  final int rows = Configurator.Field.NUM_ROWS;
  
  Cell[][] cells = new Cell[rows][cols];  

  CellField() {
    this.initCells();
    
    println("Cells ready");
  }    
  
  private void initCells() {
    for (int i = 0; i < this.rows; i++) {
      for (int j = 0; j < this.cols; j++) {
        cells[i][j] = new Cell(j * Configurator.Cell.WIDTH, i * Configurator.Cell.HEIGHT);
      }
    }
  }

  /**
  * Use this function to draw cells
  */
  public void draw() {
    for (int i = 0; i < this.rows; i++) {
      for (int j = 0; j < this.cols; j++) {
        this.cells[i][j].draw();
      }
    }
  }

  /**
  * Use this function to find a cell for a moving pixel
  */
  public void findCellFor(MovingPixel movingPixel) {
    int i = 0;
    int j = 0;
    
    synchronized(movingPixel) {   
      i = movingPixel.currentY / Configurator.Cell.HEIGHT;
      j = movingPixel.currentX / Configurator.Cell.WIDTH;
    }
    
    if (!range(i, 0, cellField.rows) || !range(j, 0, cellField.cols)) {
      return;
    }
    
    try {
      if (cellField.cells[i][j].matchPixel(movingPixel)) {
        cellField.cells[i][j].onPixelEnter(movingPixel);
        }
    } catch(Exception e) {
      e.printStackTrace();
    }
  }
 
  public boolean range(int victim, int val1, int val2) {
    return(victim >= val1 && victim < val2);
  }   
}





static class Configurator{    
  static class Pixel {
    final static int WIDTH = 7;
    final static int HEIGHT = 7;
    
    final static int MAX_QUANTITY = 1000000;
    final static int START_QUANTITY_MUL = 100;//start quantity = original * START_QUANTITY_MUL   
    final static int SPEED_MUL = 100;//speed = original * SPEED_MUL   
    final static int TRAJECTORY_CHANGE_PERIOD = 1800;
    final static int MATCH_TIMER_PERIOD = 100;     
    
    final static boolean FILL_PIXEL = false;
  }
   
  static class Cell {  
    final static int WIDTH  = 105;
    final static int HEIGHT = 45;    
 
    final static boolean VISIBLE_BORDER       = false;//set true if you want to see borders between cells
    final static boolean REACT_ON_PIXEL_ENTER = false;//set false if you want cells not became red when pixel enters
  }

  static class Field {
    final static int NUM_COLS = 10;
    final static int NUM_ROWS = 10;    
  } 
  
  static class Sound {    
    final static int MAX_SOUNDS = 10;
    final static int PLAYLIST_TIMER_PERIOD = 50;   

    final static boolean AVALAIBLE = true;
  }
}

class MovingPixel {
  private Cell cell = null; 
  
  private int startX = 0;
  private int startY = 0;   
  private double destX = 0;
  private double destY = 0;

  private int currentX = 0;
  private int currentY = 0; 
      
  private int startTime = 0;
  private int TTL = 0;  
  
  private int pixelWidth = Configurator.Pixel.WIDTH;
  private int pixelHeight = Configurator.Pixel.HEIGHT;
  
  public MovingPixel() {
    this.trajectory1();
    
    this.startTime = millis();   
    this.TTL = 80000 / Configurator.Pixel.SPEED_MUL + PApplet.parseInt(3 + random(60000 / Configurator.Pixel.SPEED_MUL));     
  }
  
  /**
  * ALWAYS call this function when change movement trajectory!!!
  */
  public void initMovementParams(int time) {
    this.startTime = time;
  }
  
  /**
  * Functions for initiating trajectory
  */
  
  public void startInit() {
    this.startX = PApplet.parseInt(random(width));
    this.startY = PApplet.parseInt(random(height));
    this.destX = PApplet.parseInt(random(width));
    this.destY = PApplet.parseInt(random(height));
  }
      
  public void trajectory1() {
    if ( this.currentX == 0 && this.currentY == 0) {
      this.startX = PApplet.parseInt(random(width));
      this.startY = PApplet.parseInt(random(height));
    } else {
      this.startX = this.currentX;
      this.startY = this.currentY;
    }
    this.destX = PApplet.parseInt(random(width));
    this.destY = PApplet.parseInt(random(height));
  }
  
  public void trajectory2() {
    switch(PApplet.parseInt(random(3))) {
      case(0) : {
        if ( this.currentX == 0 && this.currentY == 0) {
          this.startX = PApplet.parseInt(random(width / 3));
          this.startY = PApplet.parseInt(random(height / 1.5f));
        } else {
          this.startX = this.currentX;
          this.startY = this.currentY;
        }
        this.destX = PApplet.parseInt(-width);
        this.destY = PApplet.parseInt(width * 3);
      }
      case 1 : {
        if ( this.currentX == 0 && this.currentY == 0) {
          this.startX = PApplet.parseInt(random(width / 3));
          this.startY = PApplet.parseInt(random(height / 3));
        } else {
          this.startX = this.currentX;
          this.startY = this.currentY;
        }
        this.destX = PApplet.parseInt(width);
        this.destY = PApplet.parseInt(height / 1.5f);
        break;
      }
      case 2 : {
        if ( this.currentX == 0 && this.currentY == 0) {
          this.startX = PApplet.parseInt(random(width));
          this.startY = PApplet.parseInt(100 + random(height - 100));
        } else {
          this.startX = this.currentX;
          this.startY = this.currentY;
        }
        this.destX = PApplet.parseInt(0);
        this.destY = PApplet.parseInt(width * 1.5f);
        break;
      } 
      case 3 : {
        if ( this.currentX == 0 && this.currentY == 0) {
          this.startX = PApplet.parseInt(random(width));
          this.startY = PApplet.parseInt(random(height / 1.5f));
        } else {
          this.startX = this.currentX;
          this.startY = this.currentY;
        }
        this.destX = PApplet.parseInt(-width);
        this.destY = PApplet.parseInt(height * 3);
        break;
      }
    }   
  }
  
  public void trajectory3() {
    if ( this.currentX == 0 && this.currentY == 0) {
      this.startX = PApplet.parseInt(random(-2 * width));
      this.startY = PApplet.parseInt(random(2 * height));
    } else {
      this.startX = this.currentX;
      this.startY = this.currentY;
    }
    this.destX = PApplet.parseInt(width * 1.25f);
    this.destY = PApplet.parseInt(-height + random(height));
  }
  
  public void trajectory4() {
    if ( this.currentX == 0 && this.currentY == 0) {
      this.startX = PApplet.parseInt(random(width));
      this.startY = PApplet.parseInt(random(height * 2));
    } else {
      this.startX = this.currentX;
      this.startY = this.currentY;
    }
    this.destX = PApplet.parseInt(random(width));
    this.destY = PApplet.parseInt(random(10));
  }
  
  public void trajectory5() {
    if ( this.currentX == 0 && this.currentY == 0) {
    while(this.startX > width / 4 && this.startX < width / 1.5f) this.startX = PApplet.parseInt(random(width));
    while(this.startY > height / 4 && this.startY < height / 1.5f) this.startY = PApplet.parseInt(random(height));
    } else {
      this.startX = this.currentX;
      this.startY = this.currentY;
    }
    this.destX = PApplet.parseInt(random(width));
    this.destY = PApplet.parseInt(random(height));
  }
  
  public void trajectory6() {
    if ( this.currentX == 0 && this.currentY == 0) {
    this.startX = PApplet.parseInt(width / 2 + random(width / 2));
    this.startY = PApplet.parseInt(random(height));
    } else {
      this.startX = this.currentX;
      this.startY = this.currentY;
    }
    this.destX = PApplet.parseInt(random(width * 2));
    this.destY = PApplet.parseInt(random(height * 2));
  } 

  public void trajectory7() {
    if ( this.currentX == 0 && this.currentY == 0) {
    this.startX = PApplet.parseInt(random(width));
    this.startY = PApplet.parseInt(random(-height));
    } else {
      this.startX = this.currentX;
      this.startY = this.currentY;
    }
    this.destX = PApplet.parseInt(this.startX);
    this.destY = PApplet.parseInt(random(height * 5));
  } 

  public void trajectory8() {
    if ( this.currentX == 0 && this.currentY == 0) {
    this.startX = PApplet.parseInt(325 + random(width + 25));
    this.startY = PApplet.parseInt(0);
    } else {
      this.startX = this.currentX;
      this.startY = this.currentY;
    }
    this.destX = PApplet.parseInt(-100);
    this.destY = PApplet.parseInt(height + 40);
  }

  public void trajectory9() { 
    if ( this.currentX == 0 && this.currentY == 0) {   
    this.startX = PApplet.parseInt(100 + random(width / 2 - 100));
    this.startY = PApplet.parseInt(100 + random(height / 2 - 100));
    } else {
      this.startX = this.currentX;
      this.startY = this.currentY;
    }
    this.destX = PApplet.parseInt((int)random(2) == 1 ? 1 : width);
    this.destY = PApplet.parseInt((int)random(2) == 1 ? 1 : height);
  }

  public void trajectory10() {
    if ( this.currentX == 0 && this.currentY == 0) {
      this.startX = PApplet.parseInt(random(width / 1.5f));
      this.startY = PApplet.parseInt(random(height / 1.5f));
    } else {
      this.startX = this.currentX;
      this.startY = this.currentY;
    }
    if (this.startX > width / 2) this.destX = PApplet.parseInt(width);
    else this.destX = PApplet.parseInt (width / 2);
    if (this.startY > height / 2) this.destY = PApplet.parseInt(height);
    else this.destY = PApplet.parseInt(height / 2);
  }    
   
  /**
  * Use this function to process pixel coords movement
  */
  public void processPixel(int time) {   
    if (this.startTime + this.TTL < time) {
      this.startTime = time;
    } else {   
      float d = PApplet.parseFloat (time - this.startTime) / PApplet.parseFloat(this.TTL);
      synchronized(this) {
        this.currentX = (int)lerp(this.startX, (int)this.destX, d);
        this.currentY = (int)lerp(this.startY, (int)this.destY, d);  
      }
    } 
  }
  
  /**
  * Use this function check, the pixel matches cell
  */  
  private void checkCell() {   
    if (cell == null || !cell.matchPixel(this)) {
      cellField.findCellFor(this);
    }
  }
  
  /**
  * Use this function to draw pixel
  */ 
  private void draw(int time) {
    synchronized(this) {
      if (this.startTime + this.TTL < time) {
        return;
      }
    }

//    if(this.currentX == 0 && this.currentY == 0) {
////      this.trajectory1();
//      return;
//    };
    
    if(Configurator.Pixel.FILL_PIXEL) {
      fill(0);
    } else {
      noFill();
      stroke(0);
    }
    rect(this.currentX,
         this.currentY,
         this.pixelWidth,
         this.pixelHeight
    );
  }

  public boolean range(int victim, int val1, int val2) {
    return(victim >= val1 && victim < val2);
  }  
}


class PixelController {
  MovingPixel[] mp = null;
  Timer checkTimer = null;
  Timer trajectoryTimer = null;
  Class cl = null;

  int numProcessedPixels = 0;  
  
  public PixelController() {
    this.mp = new MovingPixel[Configurator.Pixel.MAX_QUANTITY ];
    this.checkTimer = new Timer();
    this.trajectoryTimer = new Timer();
        
    for (int i = 0; i < mp.length; i++) {
      this.mp[i] = new MovingPixel();
    }
       
    try {
      cl = Class.forName("Pythagoras$MovingPixel");
    } catch(Exception e) {
      println("Pythagoras$MovingPixel");
    } finally {
    }
    
    this.checkTimer.scheduleAtFixedRate(new TimerTask() {
      public void run() {
        for (int i = 0; i < numProcessedPixels; i++) {
          mp[i].checkCell();      
        }
      }		
    }, Configurator.Pixel.MATCH_TIMER_PERIOD, Configurator.Pixel.MATCH_TIMER_PERIOD);
    
    this.initTrajectoryTimer();
    
    println("Pixels ready");  
  }
  
  private void initTrajectoryTimer() {
      this.trajectoryTimer.scheduleAtFixedRate(new TimerTask() {   
      Method method = null;
      Method[] methods = new Method[10];
      
      private void initMethods() {
        for (int i = 0; i < methods.length; i++) {
          try {
          methods[i] = cl.getMethod("trajectory" + (i + 1));
          } catch(Exception e) {
          }
        }  
      }
      
      {
        println("TimerTask loaded");
        initMethods();  
      }
      
      public void run() { 
        int time = millis(); 
        
        method = methods[(int)random(methods.length - 1) + 1];
      
        for (int i = 0; i < numProcessedPixels; i++) {
          try {
            mp[i].initMovementParams(time);
            method.invoke(mp[i]);  
          } catch(Exception e) {
          }   
        }
      }		
    }, Configurator.Pixel.TRAJECTORY_CHANGE_PERIOD, Configurator.Pixel.TRAJECTORY_CHANGE_PERIOD);
  }
  
  /**
  * Use this function to display pixels
  */
  private void process() {    
    int time = millis();
    this.numProcessedPixels = time / 1000 * Configurator.Pixel.START_QUANTITY_MUL;
    
    if (numProcessedPixels > mp.length) {//check the range
      this.numProcessedPixels = mp.length;
    }
          
    for (int i = 0; i < numProcessedPixels; i++) {
      this.mp[i].processPixel(time);  
      this.mp[i].draw(time);
    }
  }
  
  /**
  * Call this function to stop the timer
  */
  
  public void stop() {
    this.trajectoryTimer.cancel();
    this.checkTimer.cancel();
  }
}














CellField cellField = null;
SoundMaster soundMaster = null;
PixelController pixelController = null;

public void setup() {     
  size(Configurator.Field.NUM_COLS * Configurator.Cell.WIDTH,
       Configurator.Field.NUM_ROWS * Configurator.Cell.HEIGHT, OPENGL);  
  
  cellField = new CellField();
  soundMaster = new SoundMaster(this);
  pixelController = new PixelController(); 
}

public void draw() {  

  background(255);
  cellField.draw();  
  pixelController.process();
}

public void stop()
{
  soundMaster.stop();
  super.stop();
}



class SoundMaster {
  Minim minim = null;
  Sound[] soundDataBase = null;
  List<Sound> players = null;
  List<Sound> playedSounds = null;
  Timer playerCheckerTimer = null;

  SoundMaster(PApplet pApplet) {
    this.minim = new Minim(pApplet);
    this.soundDataBase = new Sound[cellField.rows * cellField.cols];
    this.players = Collections.synchronizedList(new LinkedList());
    this.playedSounds = Collections.synchronizedList(new LinkedList());     
    this.playerCheckerTimer = new Timer();
  
    this.playerCheckerTimer.scheduleAtFixedRate(new TimerTask() {
      public void run() {
        play();
	clearPlaylist();
      }		
    }, Configurator.Sound.PLAYLIST_TIMER_PERIOD, Configurator.Sound.PLAYLIST_TIMER_PERIOD);

    this.initSounds();
    
    println("Sounds inited");
  }
 
  private void stop() {
      this.minim.stop();
      this.playerCheckerTimer.cancel();
  } 
    
  private void initSounds() {     
    for (int i = 0; i < cellField.rows * cellField.cols; i++) {
      String soundName = null;
      soundName = "Sound//sound" + (i + 1) + ".wav";
      try {
//        println("Num sounds: " + soundDataBase.length);
        soundDataBase[i] = new Sound( minim.loadFile(soundName, 4096));
      } catch(Exception e) {
//       println(soundName + " "soundDataBase[i]);
      }
    }    
  }   
  
  /**
  * Use this function to play sound
  */
  public synchronized void addSoundToPlaylist() {
    synchronized(this) {    
      try {
        if (this.players.size() <= Configurator.Sound.MAX_SOUNDS) {     
          Sound player = soundDataBase[getRandomSound()];
          if (player != null) {
              this.players.add(player);
//              println(player + "added to playlist");
              player.justAddedToPlaylist = true;
            }
          else {
//            println("No sound for this cell");
          }
        }  
      } catch(Exception e) {
        e.printStackTrace();
      }
    }
  }
  
  /**
  * Use this function to play sound by your Player
  */
  public synchronized void play() {
    if(!Configurator.Sound.AVALAIBLE) {
      return;
    }

    synchronized(this) {   
      try {
        for (Sound player : this.players) {  
            if(!player.isPlaying() && player.justAddedToPlaylist) {           
              player.play();
              player.justAddedToPlaylist = false;      
            }    
        }            
      } catch(Exception e) {
          e.printStackTrace();
      }
    }    
  }
  
  /**
  * Use this function to remove played sounds
  */
  public void clearPlaylist() {
    synchronized(this) {
      for (Sound player : this.players) {
        if (!player.isPlaying() && !player.justAddedToPlaylist) {
          this.playedSounds.add(player);  
        }
      }
    
      for (Sound player : this.playedSounds) {
//        println("sound removed"); 
        this.players.remove(player);  
      }
        
      this.playedSounds.clear(); 
    }   
  }
    
  /**
  * Use this function to get random position sound
  */
  private int getRandomSound() {  
    return (int)random(soundDataBase.length - 1) + 1;
  } 
}

/**
* This class is used as wrapper for Processing.AudioPlayer
* His main feature is that he REMEMBER his adding to playlist
*/
class Sound {
  AudioPlayer player = null;
  boolean justAddedToPlaylist = true;
  
  Sound(AudioPlayer player) {
    this.player = player;
  }
  
  /**
  * Use this function to play sound
  */
  public void play() {   
    if(this.justAddedToPlaylist) {
      if (this.player == null) {
        println("WTF!!! AudioPlayer can't == null!!!!");
        return;
      }            
      this.player.play();
    }
  }
  
  /**
  * Use this function to check - is the sound playing?
  */
  public boolean isPlaying() {
    return this.player.isPlaying();
  }
}


    static public void main(String args[]) {
        PApplet.main(new String[] { "--bgcolor=#ECE9D8", "Pythagoras" });
    }
}
