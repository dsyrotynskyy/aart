class MovingPixel {
  private Cell cell = null; 
  
  private int startX = 0;
  private int startY = 0;   
  private double destX = 0;
  private double destY = 0;

  private int currentX = 0;
  private int currentY = 0; 
      
  private int startTime = 0;
  private int TTL = 0;  
  
  private int pixelWidth = Configurator.Pixel.WIDTH;
  private int pixelHeight = Configurator.Pixel.HEIGHT;
  
  public MovingPixel() {
    this.trajectory1();
    
    this.startTime = millis();   
    this.TTL = 80000 / Configurator.Pixel.SPEED_MUL + int(3 + random(60000 / Configurator.Pixel.SPEED_MUL));     
  }
  
  /**
  * ALWAYS call this function when change movement trajectory!!!
  */
  public void initMovementParams(int time) {
    this.startTime = time;
  }
  
  /**
  * Functions for initiating trajectory
  */
  
  public void startInit() {
    this.startX = int(random(width));
    this.startY = int(random(height));
    this.destX = int(random(width));
    this.destY = int(random(height));
  }
      
  public void trajectory1() {
    if ( this.currentX == 0 && this.currentY == 0) {
      this.startX = int(random(width));
      this.startY = int(random(height));
    } else {
      this.startX = this.currentX;
      this.startY = this.currentY;
    }
    this.destX = int(random(width));
    this.destY = int(random(height));
  }
  
  public void trajectory2() {
    switch(int(random(3))) {
      case(0) : {
        if ( this.currentX == 0 && this.currentY == 0) {
          this.startX = int(random(width / 3));
          this.startY = int(random(height / 1.5));
        } else {
          this.startX = this.currentX;
          this.startY = this.currentY;
        }
        this.destX = int(-width);
        this.destY = int(width * 3);
      }
      case 1 : {
        if ( this.currentX == 0 && this.currentY == 0) {
          this.startX = int(random(width / 3));
          this.startY = int(random(height / 3));
        } else {
          this.startX = this.currentX;
          this.startY = this.currentY;
        }
        this.destX = int(width);
        this.destY = int(height / 1.5);
        break;
      }
      case 2 : {
        if ( this.currentX == 0 && this.currentY == 0) {
          this.startX = int(random(width));
          this.startY = int(100 + random(height - 100));
        } else {
          this.startX = this.currentX;
          this.startY = this.currentY;
        }
        this.destX = int(0);
        this.destY = int(width * 1.5);
        break;
      } 
      case 3 : {
        if ( this.currentX == 0 && this.currentY == 0) {
          this.startX = int(random(width));
          this.startY = int(random(height / 1.5));
        } else {
          this.startX = this.currentX;
          this.startY = this.currentY;
        }
        this.destX = int(-width);
        this.destY = int(height * 3);
        break;
      }
    }   
  }
  
  void trajectory3() {
    if ( this.currentX == 0 && this.currentY == 0) {
      this.startX = int(random(-2 * width));
      this.startY = int(random(2 * height));
    } else {
      this.startX = this.currentX;
      this.startY = this.currentY;
    }
    this.destX = int(width * 1.25);
    this.destY = int(-height + random(height));
  }
  
  void trajectory4() {
    if ( this.currentX == 0 && this.currentY == 0) {
      this.startX = int(random(width));
      this.startY = int(random(height * 2));
    } else {
      this.startX = this.currentX;
      this.startY = this.currentY;
    }
    this.destX = int(random(width));
    this.destY = int(random(10));
  }
  
  void trajectory5() {
    if ( this.currentX == 0 && this.currentY == 0) {
    while(this.startX > width / 4 && this.startX < width / 1.5) this.startX = int(random(width));
    while(this.startY > height / 4 && this.startY < height / 1.5) this.startY = int(random(height));
    } else {
      this.startX = this.currentX;
      this.startY = this.currentY;
    }
    this.destX = int(random(width));
    this.destY = int(random(height));
  }
  
  void trajectory6() {
    if ( this.currentX == 0 && this.currentY == 0) {
    this.startX = int(width / 2 + random(width / 2));
    this.startY = int(random(height));
    } else {
      this.startX = this.currentX;
      this.startY = this.currentY;
    }
    this.destX = int(random(width * 2));
    this.destY = int(random(height * 2));
  } 

  void trajectory7() {
    if ( this.currentX == 0 && this.currentY == 0) {
    this.startX = int(random(width));
    this.startY = int(random(-height));
    } else {
      this.startX = this.currentX;
      this.startY = this.currentY;
    }
    this.destX = int(this.startX);
    this.destY = int(random(height * 5));
  } 

  void trajectory8() {
    if ( this.currentX == 0 && this.currentY == 0) {
    this.startX = int(325 + random(width + 25));
    this.startY = int(0);
    } else {
      this.startX = this.currentX;
      this.startY = this.currentY;
    }
    this.destX = int(-100);
    this.destY = int(height + 40);
  }

  void trajectory9() { 
    if ( this.currentX == 0 && this.currentY == 0) {   
    this.startX = int(100 + random(width / 2 - 100));
    this.startY = int(100 + random(height / 2 - 100));
    } else {
      this.startX = this.currentX;
      this.startY = this.currentY;
    }
    this.destX = int((int)random(2) == 1 ? 1 : width);
    this.destY = int((int)random(2) == 1 ? 1 : height);
  }

  void trajectory10() {
    if ( this.currentX == 0 && this.currentY == 0) {
      this.startX = int(random(width / 1.5));
      this.startY = int(random(height / 1.5));
    } else {
      this.startX = this.currentX;
      this.startY = this.currentY;
    }
    if (this.startX > width / 2) this.destX = int(width);
    else this.destX = int (width / 2);
    if (this.startY > height / 2) this.destY = int(height);
    else this.destY = int(height / 2);
  }    
   
  /**
  * Use this function to process pixel coords movement
  */
  void processPixel(int time) {   
    if (this.startTime + this.TTL < time) {
      this.startTime = time;
    } else {   
      float d = float (time - this.startTime) / float(this.TTL);
      synchronized(this) {
        this.currentX = (int)lerp(this.startX, (int)this.destX, d);
        this.currentY = (int)lerp(this.startY, (int)this.destY, d);  
      }
    } 
  }
  
  /**
  * Use this function check, the pixel matches cell
  */  
  private void checkCell() {   
    if (cell == null || !cell.matchPixel(this)) {
      cellField.findCellFor(this);
    }
  }
  
  /**
  * Use this function to draw pixel
  */ 
  private void draw(int time) {
    synchronized(this) {
      if (this.startTime + this.TTL < time) {
        return;
      }
    }

//    if(this.currentX == 0 && this.currentY == 0) {
////      this.trajectory1();
//      return;
//    };
    
    if(Configurator.Pixel.FILL_PIXEL) {
      fill(0);
    } else {
      noFill();
      stroke(0);
    }
    rect(this.currentX,
         this.currentY,
         this.pixelWidth,
         this.pixelHeight
    );
  }

  boolean range(int victim, int val1, int val2) {
    return(victim >= val1 && victim < val2);
  }  
}


class PixelController {
  MovingPixel[] mp = null;
  Timer checkTimer = null;
  Timer trajectoryTimer = null;
  Class cl = null;

  int numProcessedPixels = 0;  
  
  public PixelController() {
    this.mp = new MovingPixel[Configurator.Pixel.MAX_QUANTITY ];
    this.checkTimer = new Timer();
    this.trajectoryTimer = new Timer();
        
    for (int i = 0; i < mp.length; i++) {
      this.mp[i] = new MovingPixel();
    }
       
    try {
      cl = Class.forName("Pythagoras$MovingPixel");
    } catch(Exception e) {
      println("Pythagoras$MovingPixel");
    } finally {
    }
    
    this.checkTimer.scheduleAtFixedRate(new TimerTask() {
      public void run() {
        for (int i = 0; i < numProcessedPixels; i++) {
          mp[i].checkCell();      
        }
      }		
    }, Configurator.Pixel.MATCH_TIMER_PERIOD, Configurator.Pixel.MATCH_TIMER_PERIOD);
    
    this.initTrajectoryTimer();
    
    println("Pixels ready");  
  }
  
  private void initTrajectoryTimer() {
      this.trajectoryTimer.scheduleAtFixedRate(new TimerTask() {   
      Method method = null;
      Method[] methods = new Method[10];
      
      private void initMethods() {
        for (int i = 0; i < methods.length; i++) {
          try {
          methods[i] = cl.getMethod("trajectory" + (i + 1));
          } catch(Exception e) {
          }
        }  
      }
      
      {
        println("TimerTask loaded");
        initMethods();  
      }
      
      public void run() { 
        int time = millis(); 
        
        method = methods[(int)random(methods.length - 1) + 1];
      
        for (int i = 0; i < numProcessedPixels; i++) {
          try {
            mp[i].initMovementParams(time);
            method.invoke(mp[i]);  
          } catch(Exception e) {
          }   
        }
      }		
    }, Configurator.Pixel.TRAJECTORY_CHANGE_PERIOD, Configurator.Pixel.TRAJECTORY_CHANGE_PERIOD);
  }
  
  /**
  * Use this function to display pixels
  */
  private void process() {    
    int time = millis();
    this.numProcessedPixels = time / 1000 * Configurator.Pixel.START_QUANTITY_MUL;
    
    if (numProcessedPixels > mp.length) {//check the range
      this.numProcessedPixels = mp.length;
    }
          
    for (int i = 0; i < numProcessedPixels; i++) {
      this.mp[i].processPixel(time);  
      this.mp[i].draw(time);
    }
  }
  
  /**
  * Call this function to stop the timer
  */
  
  public void stop() {
    this.trajectoryTimer.cancel();
    this.checkTimer.cancel();
  }
}





