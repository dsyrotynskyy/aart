
import ddf.minim.*;
import ddf.minim.signals.*;
import ddf.minim.analysis.*;
import ddf.minim.effects.*;
import processing.opengl.*;
import java.lang.reflect.Method;
import java.util.*;

CellField cellField = null;
SoundMaster soundMaster = null;
PixelController pixelController = null;

void setup() {     
  size(Configurator.Field.NUM_COLS * Configurator.Cell.WIDTH,
       Configurator.Field.NUM_ROWS * Configurator.Cell.HEIGHT, OPENGL);  
  
  cellField = new CellField();
  soundMaster = new SoundMaster(this);
  pixelController = new PixelController(); 
}

void draw() {  

  background(255);
  cellField.draw();  
  pixelController.process();
}

void stop()
{
  soundMaster.stop();
  super.stop();
}

