
class Cell {
  private int x = 0;
  private int y = 0;
  
  private int entered = 0; //for test
  
  Cell(int x, int y) {
    this.x = x;
    this.y = y;
  } 
  
  /**
  * Use function to draw the cell
  */
  void draw() {
    if (Configurator.Cell.REACT_ON_PIXEL_ENTER && this.entered > 0) {
      this.entered--;
      fill(223, 0, 0);
    } else {
      noFill();
    }
    
    if (!Configurator.Cell.VISIBLE_BORDER) {
      noStroke();
    }
    
    rect(x,
         y,
         Configurator.Cell.WIDTH,
         Configurator.Cell.HEIGHT
    );
  }
  
  /**
  * Call this function when pixel enters cell
  */
  void onPixelEnter(MovingPixel movingPixel) {
    this.entered = 50;//for test
    
    movingPixel.cell = this;
    soundMaster.addSoundToPlaylist();
  }

  /**
  * Use this function to match a moving pixel in the cell
  */
  boolean matchPixel(MovingPixel movingPixel) {
    return range(movingPixel.currentX + movingPixel.pixelWidth / 2, x, x + Configurator.Cell.WIDTH) && 
           range(movingPixel.currentY + movingPixel.pixelHeight / 2, y, y + Configurator.Cell.HEIGHT) ? true : false;
  }

  /**
    * Use to check range of victim between val1 nad val2
    */
  boolean range(int victim, int val1, int val2) {
    return(victim >= val1 && victim < val2);
  }  
}

class CellField {  
  final int cols = Configurator.Field.NUM_COLS;
  final int rows = Configurator.Field.NUM_ROWS;
  
  Cell[][] cells = new Cell[rows][cols];  

  CellField() {
    this.initCells();
    
    println("Cells ready");
  }    
  
  private void initCells() {
    for (int i = 0; i < this.rows; i++) {
      for (int j = 0; j < this.cols; j++) {
        cells[i][j] = new Cell(j * Configurator.Cell.WIDTH, i * Configurator.Cell.HEIGHT);
      }
    }
  }

  /**
  * Use this function to draw cells
  */
  void draw() {
    for (int i = 0; i < this.rows; i++) {
      for (int j = 0; j < this.cols; j++) {
        this.cells[i][j].draw();
      }
    }
  }

  /**
  * Use this function to find a cell for a moving pixel
  */
  void findCellFor(MovingPixel movingPixel) {
    int i = 0;
    int j = 0;
    
    synchronized(movingPixel) {   
      i = movingPixel.currentY / Configurator.Cell.HEIGHT;
      j = movingPixel.currentX / Configurator.Cell.WIDTH;
    }
    
    if (!range(i, 0, cellField.rows) || !range(j, 0, cellField.cols)) {
      return;
    }
    
    try {
      if (cellField.cells[i][j].matchPixel(movingPixel)) {
        cellField.cells[i][j].onPixelEnter(movingPixel);
        }
    } catch(Exception e) {
      e.printStackTrace();
    }
  }
 
  boolean range(int victim, int val1, int val2) {
    return(victim >= val1 && victim < val2);
  }   
}




