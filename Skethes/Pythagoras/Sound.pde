
class SoundMaster {
  Minim minim = null;
  Sound[] soundDataBase = null;
  List<Sound> players = null;
  List<Sound> playedSounds = null;
  Timer playerCheckerTimer = null;

  SoundMaster(PApplet pApplet) {
    this.minim = new Minim(pApplet);
    this.soundDataBase = new Sound[cellField.rows * cellField.cols];
    this.players = Collections.synchronizedList(new LinkedList());
    this.playedSounds = Collections.synchronizedList(new LinkedList());     
    this.playerCheckerTimer = new Timer();
  
    this.playerCheckerTimer.scheduleAtFixedRate(new TimerTask() {
      public void run() {
        play();
	clearPlaylist();
      }		
    }, Configurator.Sound.PLAYLIST_TIMER_PERIOD, Configurator.Sound.PLAYLIST_TIMER_PERIOD);

    this.initSounds();
    
    println("Sounds inited");
  }
 
  private void stop() {
      this.minim.stop();
      this.playerCheckerTimer.cancel();
  } 
    
  private void initSounds() {     
    for (int i = 0; i < cellField.rows * cellField.cols; i++) {
      String soundName = null;
      soundName = "Sound//sound" + (i + 1) + ".wav";
      try {
//        println("Num sounds: " + soundDataBase.length);
        soundDataBase[i] = new Sound( minim.loadFile(soundName, 4096));
      } catch(Exception e) {
//       println(soundName + " "soundDataBase[i]);
      }
    }    
  }   
  
  /**
  * Use this function to play sound
  */
  public synchronized void addSoundToPlaylist() {
    synchronized(this) {    
      try {
        if (this.players.size() <= Configurator.Sound.MAX_SOUNDS) {     
          Sound player = soundDataBase[getRandomSound()];
          if (player != null) {
              this.players.add(player);
//              println(player + "added to playlist");
              player.justAddedToPlaylist = true;
            }
          else {
//            println("No sound for this cell");
          }
        }  
      } catch(Exception e) {
        e.printStackTrace();
      }
    }
  }
  
  /**
  * Use this function to play sound by your Player
  */
  public synchronized void play() {
    if(!Configurator.Sound.AVALAIBLE) {
      return;
    }

    synchronized(this) {   
      try {
        for (Sound player : this.players) {  
            if(!player.isPlaying() && player.justAddedToPlaylist) {           
              player.play();
              player.justAddedToPlaylist = false;      
            }    
        }            
      } catch(Exception e) {
          e.printStackTrace();
      }
    }    
  }
  
  /**
  * Use this function to remove played sounds
  */
  public void clearPlaylist() {
    synchronized(this) {
      for (Sound player : this.players) {
        if (!player.isPlaying() && !player.justAddedToPlaylist) {
          this.playedSounds.add(player);  
        }
      }
    
      for (Sound player : this.playedSounds) {
//        println("sound removed"); 
        this.players.remove(player);  
      }
        
      this.playedSounds.clear(); 
    }   
  }
    
  /**
  * Use this function to get random position sound
  */
  private int getRandomSound() {  
    return (int)random(soundDataBase.length - 1) + 1;
  } 
}

/**
* This class is used as wrapper for Processing.AudioPlayer
* His main feature is that he REMEMBER his adding to playlist
*/
class Sound {
  AudioPlayer player = null;
  boolean justAddedToPlaylist = true;
  
  Sound(AudioPlayer player) {
    this.player = player;
  }
  
  /**
  * Use this function to play sound
  */
  void play() {   
    if(this.justAddedToPlaylist) {
      if (this.player == null) {
        println("WTF!!! AudioPlayer can't == null!!!!");
        return;
      }            
      this.player.play();
    }
  }
  
  /**
  * Use this function to check - is the sound playing?
  */
  boolean isPlaying() {
    return this.player.isPlaying();
  }
}

