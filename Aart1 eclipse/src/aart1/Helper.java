package aart1;

import processing.core.PApplet;

class Timer {
	public int savedTime;
	public int totalTime = 1000;

	public Timer() {
		savedTime = (int) System.currentTimeMillis();
	}

	public Timer(int totalTime) {
		this();
		this.totalTime = totalTime;
	}

	public boolean isTime() {
		int passedTime = (int) System.currentTimeMillis() - savedTime;
		if (passedTime > totalTime) {
			savedTime = (int) System.currentTimeMillis(); // Save the current time to restart the
									// timer!
			return true;
		}
		return false;
	}

	void forceTime() {
		savedTime -= totalTime;
	}

	void restart() {
		savedTime = (int) System.currentTimeMillis();
	}
}

class Time
{

	private static long currentTime;
	private static long previousTime;
	private static double deltaTime;
	private static double countTime;	
	private float timeScale = 1.0f;

	PApplet parent;

	@SuppressWarnings("deprecation")
	public Time(PApplet _parent)
	{

		parent = _parent;

		parent.registerPre(this);

		previousTime = currentTime;

		currentTime = System.currentTimeMillis();
		
		deltaTime = 0;
		
		countTime = 0;
	}

	/**
	 * Called by the PApplet before draw(). 
	 * NOTE: This method is automatic and should not be called by the user. 
	 */
	public void pre()
	{

		previousTime = currentTime;

		currentTime = System.currentTimeMillis(); 
		
		// remember kids: time is counted as a long with 1000 to a second (millisecond)
		// hense the 0.001f to get the float value of deltaTime.
		deltaTime = (currentTime - previousTime) * 0.001d * timeScale;
		
		countTime += deltaTime;

	}
	
	public static float getDeltaTime() 
	{
		return (float)deltaTime;
	}

	public float getTimeScale() {
		return timeScale;
	}

	public void setTimeScale(float timeScale) {
		this.timeScale = timeScale;
	}

	public static float getCountTime() {
		return (float)countTime;
	}
	
	public void resetTime(){
		
	}
}
