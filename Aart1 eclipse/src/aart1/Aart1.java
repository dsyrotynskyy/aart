package aart1;

import java.util.ArrayList;
import java.util.HashMap;

import processing.core.PApplet;
import processing.core.PImage;

public class Aart1 extends PApplet {
	public static void main(String args[]) {
		PApplet.main(new String[] { "aart1.Aart1" });
	}

	int STRIPE_COUNT = 25;
	Stripe baseStripe;
	StripeController stripeController;
	PImage back;

	public void setup() {
		// size (1280, 1024);
		size(640, 640);
		initStripes();

		currentTime = System.currentTimeMillis();
		previousTime = currentTime;
		deltaTime = 0;
	}

	void initStripes() {
		baseStripe = new Stripe();
		baseStripe.myWidth = 18f;

		stripeController = new StripeController(baseStripe, this, STRIPE_COUNT);
	}

	private static long currentTime;
	private static long previousTime;
	private static float deltaTime = -1;

	public void draw() {
		calculateTimeScale();

		noStroke();
		background(255);

		pushMatrix();
		stripeController.onUpdate();
		popMatrix();

		// this.calculateFPS();
	}

	int fcount, lastm;
	float frate;
	int fint = 3;

	void calculateFPS() {
		fcount += 1;
		int m = millis();
		if (m - lastm > 1000 * fint) {
			frate = (float) (fcount) / fint;
			fcount = 0;
			lastm = m;
			println("fps: " + frate);
		}
		fill(0);
		text("fps: " + frate, 10, 20);
	}

	void calculateTimeScale() {
		previousTime = currentTime;
		currentTime = System.currentTimeMillis();

		float timeOffset = (currentTime - previousTime) * 0.001f;
		if (deltaTime > 0f) {
			deltaTime = lerp(deltaTime, timeOffset, 0.1f);
		} else {
			deltaTime = timeOffset;
		}
		// println ("delta time " + deltaTime);
	}

	float step(float v, float target, float step) {
		if (step < 0f) {
			// println("Bad arg");
		}

		if (v < target) {
			v += step;
		} else {
			v -= step;
		}
		if (abs(abs(v) - abs(target)) <= step) {
			v = target;
		}

		return v;
	}

	class Stripe {
		public boolean drawInfinite = false;
		public float breakSize = 10f;
		public float yPos;
		public float xPos;
		public float myWidth = 8f;
		public float myHeight;

		Stripe() {
		}

		Stripe(Stripe other) {
			clone(other);
		}

		Stripe clone(Stripe other) {
			this.drawInfinite = other.drawInfinite;
			this.breakSize = other.breakSize;
			this.myWidth = other.myWidth;
			this.myHeight = other.myHeight;
			this.yPos = other.yPos;
			return this;
		}

		void onDrawByStart() {
			fill(0);
			int totalNum = 5;
			for (int i = 0; i < totalNum; i++) {
				rect(xPos, yPos - (myHeight + breakSize) * i, myWidth, myHeight);

				if (drawInfinite) {
					rect(xPos, yPos + (myHeight + breakSize) * i, myWidth,
							myHeight);
				}
			}
		}
	}

	class HoriStripe {
		float xPos;
		float yPos = 1f;
		float myWidth = 5f;
		float myHeight = 1f;
		float targetRotation = 0f;

		StripeController owner;

		Timer blinkTimer;
		Timer disTimer;

		boolean dissapear = false;

		public HoriStripe(StripeController owner) {
			this.owner = owner;

			yPos = random(height);

			xPos += owner.widthOffset;
			yPos += owner.heightOffset;

			if (random(100) > 80) {
				this.blinkTimer = new Timer((int) random(250));
			}

			if (random(100) > 80) {
				this.disTimer = new Timer((int) random(250));
			}
		}

		public HoriStripe(StripeController owner, Stripe stripe) {
			this(owner);

			this.xPos += stripe.xPos;
			this.myWidth = stripe.myWidth;
			if (myWidth > 0)
				myWidth = owner.maxOffset;

			this.xPos -= stripe.myWidth / 5f;
		}

		public void onDraw() {
			fill(255);

			if (this.blinkTimer != null && this.blinkTimer.isTime()) {
				return;
			}

			if (this.disTimer != null && this.disTimer.isTime()) {
				dissapear = !dissapear;
			}

			pushMatrix();
			float x = xPos - owner.widthOffset;
			float y = yPos - owner.heightOffset;
			translate(x, y);
			rotate(radians(curRotation));

			if (!dissapear) {
				rectMode(CENTER);
				rect(0, 0, myWidth, myHeight);
				rectMode(CORNER);
			}

			popMatrix();
		}

		float xStep;
		float yStep;
		float curRotation;

		void update() {
			this.xPos += xStep;
			this.yPos += yStep;

			if (this.xPos > width) {
				this.xPos -= width;
				this.yPos = random(height);
			}

			if (this.yPos > height) {
				this.yPos -= height;
				this.xPos = random(width);
			}

			curRotation = lerp(curRotation, this.targetRotation, 10 * deltaTime);
		}
	}

	abstract class StripeBehaviour {
		Stripe baseStripe;
		StripeController owner;

		StripeBehaviour init(StripeController owner) {
			this.owner = owner;
			this.baseStripe = owner.base;
			return this;
		}

		abstract void behave();
	}

	class StripeWidthBehaviour extends StripeBehaviour {
		boolean incWidth = true;
		float desWidth;
		Timer pauseTimer = new Timer(0);
		float offsetLoop;

		StripeBehaviour init(StripeController owner) {
			super.init(owner);
			desWidth = owner.base.myWidth;
			pauseTimer.forceTime();
			return this;
		}

		void behave() {
			if (!pauseTimer.isTime()) {
				if (desWidth == 0) {
					baseStripe.myWidth = 0f;
				} else {
					pauseTimer.forceTime();
				}
				return;
			}

			if (incWidth) {
				desWidth = owner.maxOffset * 1.1f;

				if (owner.base.myWidth >= owner.maxOffset) {
					incWidth = false;
					pauseTimer.restart();
				} else {
					pauseTimer.forceTime();
				}
			} else {
				desWidth = 0;
				if (owner.base.myWidth <= 0.0f) {
					incWidth = true;
					pauseTimer.restart();
				} else {
					pauseTimer.forceTime();
				}
			}

			calcCurWidth();
		}

		void calcCurWidth() {
			float inc = (float) (15f * deltaTime);

			if (baseStripe.myWidth < desWidth) {
				baseStripe.myWidth += inc;
				// owner.widthOffset -= WIDTH_SPEED * deltaTime / 2f;
			}

			if (baseStripe.myWidth > desWidth) {
				baseStripe.myWidth -= inc * 2f;

				owner.widthOffset += inc * 2f;
				// owner.widthOffset -= WIDTH_SPEED * deltaTime / 2f;
			}
		}
	}

	Timer switchTimer = new Timer(300);

	public void mousePressed() {
		if (switchTimer.isTime()) {

			if (mouseButton == LEFT) {
				stripeController.switchScenario();
			} else {
				stripeController.switchScenarioBack();
			}
		}
	}

	public void keyPressed() {
		if (key == CODED) {
			if (keyCode == LEFT) {
				stripeController.switchScenarioBack();
			}
			if (keyCode == RIGHT) {
				stripeController.switchScenario();
			}
		}
	}

	class StripeController {
		ArrayList<HoriStripe> horiStripes = new ArrayList<HoriStripe>(50);

		Stripe[] stripes;
		float maxOffset;
		float distModif = 1f;

		Stripe base;
		int stripeIgnorePos = 0;

		float widthOffset;
		float heightOffset;
		float rotationDesValue;
		
		PApplet owner;

		StripeController(Stripe base, PApplet owner, int count) {
			this.base = base;
			this.owner = owner;
			distModif = 0f;
			baseStripe.myWidth = 0f;
			this.stripes = new Stripe[count];
			for (int i = 0; i < stripes.length; i++) {
				stripes[i] = new Stripe(base);
			}
			maxOffset = width / Math.max(1, stripes.length) * 4.5f;

			prepareScenarios();
		}

		float calcX(int i, int central) {
			return maxOffset * (i - central) * distModif;
		}

		ArrayList<StripeScenario> stripeScenarios = new ArrayList<StripeScenario>();
		int currentScenarioNum;
		StripeScenario currentScenario;

		void switchScenario() {
			for (int i = 0; i < stripeScenarios.size(); i++) {
				if (stripeScenarios.get(i) == currentScenario) {
					int nextScenarioPosition = i + 1;
					if (nextScenarioPosition >= stripeScenarios.size()) {
						nextScenarioPosition = 0;
					}
					currentScenario = stripeScenarios.get(nextScenarioPosition)
							.onStart(this);
					break;
				}
			}
		}

		void switchScenarioBack() {
			for (int i = 0; i < stripeScenarios.size(); i++) {
				if (stripeScenarios.get(i) == currentScenario) {
					int nextScenarioPosition = i - 1;
					if (nextScenarioPosition < 0) {
						nextScenarioPosition = stripeScenarios.size() - 1;
					}
					currentScenario = stripeScenarios.get(nextScenarioPosition)
							.onStart(this);
					break;
				}
			}
		}

		void onUpdate() {
			currentScenario.behave();
			rotate(radians(rotationDesValue));
			translate(widthOffset, heightOffset);

			base.onDrawByStart();

			for (int i = 0; i < stripes.length; i++) {
				if (i > stripeIgnorePos) {
					continue;
				}
				float curX = calcX(i, stripes.length / 2);
				stripes[i].xPos = curX;
				stripes[i].clone(base);
				stripes[i].onDrawByStart();
			}

			for (int i = 0; i < horiStripes.size(); i++) {
				HoriStripe s = this.horiStripes.get(i);
				if (s != null)
					s.onDraw();
			}
		}

		void prepareScenarios() {
			stripeScenarios.add(new VoidScenario());

			stripeScenarios.add(new StripeCreationScenario());
			stripeScenarios.add(new StripeMultiplicationScenario());
			stripeScenarios.add(new StripeMovementScenario());
			stripeScenarios.add(new StripeMovementWidthScenario());

			stripeScenarios.add(new Hori1Scenario_Creation());
			stripeScenarios.add(new Hori2Scenario_StopMotion());
			stripeScenarios.add(new Hori3Scenario_Falling());
			stripeScenarios.add(new Hori4Scenario_FallingRotation());
			stripeScenarios.add(new Hori5Scenario_Randomizing());
			stripeScenarios.add(new SuperHoriScenario());

			stripeScenarios.add(new StripeDeplicationScenario());

			currentScenario = stripeScenarios.get(0).onStart(this);
		}
	}

	class VoidScenario extends StripeScenario {
		void onBehave() {
			owner.horiStripes.clear();

			owner.distModif = lerp(owner.distModif, 0f, 0.15f);
			baseStripe.myWidth = lerp(baseStripe.myWidth, 0f, 0.4f);
			baseStripe.myHeight = height;
			baseStripe.yPos = lerp(baseStripe.yPos, -height, 0.1f);
		}
	}

	class StripeCreationScenario extends StripeScenario {
		void initBehavs() {
			baseStripe.drawInfinite = false;
			baseStripe.yPos = -height;
			baseStripe.breakSize = 10f;
			owner.stripeIgnorePos = 0;

			owner.heightOffset = 0f;
			owner.widthOffset = -width * 0.5f;

			owner.rotationDesValue = -90f;
		}

		void onBehave() {
			owner.distModif = lerp(owner.distModif, 0f, 0.15f);

			baseStripe.myWidth = step(baseStripe.myWidth, 2f, 0.5f);
			baseStripe.yPos += 551.5f * deltaTime;

			if (baseStripe.yPos > height * 3) {
				baseStripe.yPos -= height * 2;
			}
		}
	}

	class StripeMultiplicationScenario extends StripeScenario {
		void onBehave() {
			owner.stripeIgnorePos = owner.stripes.length / 2;
			baseStripe.drawInfinite = true;
			baseStripe.breakSize = 0f;
			baseStripe.yPos = 0f;
			baseStripe.breakSize = 0f;
			owner.distModif = lerp(owner.distModif, 1f, 0.015f);
		}
	}

	class StripeMovementScenario extends StripeScenario {
		float rotationStep = 2f;

		void initBehavs() {
			baseStripe.drawInfinite = true;
		}

		void onBehave() {
			owner.distModif = 1f;
			baseStripe.myWidth = 2f;
			baseStripe.breakSize = 0f;
			baseStripe.yPos = 0f;

			owner.rotationDesValue = step(owner.rotationDesValue, 0f,
					rotationStep);

			if (owner.rotationDesValue != 0f) {
				owner.widthOffset += 20 * WIDTH_SPEED * deltaTime;
			} else {
				owner.widthOffset += widthOffsetMovementDelta();
			}

			if (owner.widthOffset > width) {
				owner.widthOffset -= owner.maxOffset;
			}
		}
	}

	float WIDTH_SPEED = 50f;
	StripeWidthBehaviour widthBeh;

	class StripeMovementWidthScenario extends StripeScenario {

		void initBehavs() {
			owner.rotationDesValue = 0f;
			baseStripe.yPos = 0f;
			baseStripe.breakSize = 0f;

			if (widthBeh == null) {
				widthBeh = new StripeWidthBehaviour();
			}

			prepareWidthBehaviour();
		}

		void prepareWidthBehaviour() {
			stripeBehavs.add(widthBeh.init(owner));
		}

		void onBehave() {
			if (owner.widthOffset > width) {
				owner.widthOffset -= owner.maxOffset;
			}

			owner.widthOffset += widthOffsetMovementDelta();

			if (owner.widthOffset < width) {
				owner.widthOffset += owner.maxOffset;
			}
		}
	}

	float widthOffsetMovementDelta() {
		return WIDTH_SPEED * deltaTime;
	}

	int horiStripesDesCount;

	class Hori1Scenario_Creation extends StripeMovementWidthScenario {
		Timer t = new Timer(100);

		@Override
		void initBehavs() {
			super.initBehavs();
			horiStripesDesCount = 0;
			t.forceTime();
		}
		
		@Override
		void onBehave() {
			super.onBehave();

			if (t.isTime()) {
				owner.horiStripes.clear();
				horiStripesDesCount += 5;
				for (int i = 0; i < horiStripesDesCount; i++) {
					int rNum = (int) random(owner.stripes.length);
					Stripe randomStripe = owner.stripes[rNum];
					HoriStripe h = new HoriStripe(owner, randomStripe);
					h.yPos -= owner.heightOffset;
					owner.horiStripes.add(h);
				}
			}
		}
	}

	class Hori2Scenario_StopMotion extends StripeMovementWidthScenario {

		@Override
		void initBehavs() {
			super.initBehavs();

			for (int i = 0; i < horiStripesDesCount; i++) {
				int rNum = (int) random(owner.stripes.length);
				Stripe randomStripe = owner.stripes[rNum];
				HoriStripe h = new HoriStripe(owner, randomStripe);
				h.yPos -= owner.heightOffset;
				owner.horiStripes.add(h);
			}
		}
	}

	class Hori3Scenario_Falling extends StripeMovementWidthScenario {
		
		void initBehavs() {
			super.initBehavs();
			for (int i = 0; i < owner.horiStripes.size(); i++) {
				HoriStripe h = owner.horiStripes.get(i);
				h.yStep = random(1000) / 200;
			}
		}

		void onBehave() {
			super.onBehave();

			for (int i = 0; i < owner.horiStripes.size(); i++) {
				HoriStripe h = owner.horiStripes.get(i);
				if (random(100) > 90)
					h.yStep = random(1000) / 200;
				h.update();

				if (random(100) > 90) {
					h.targetRotation = 0;
				}
			}

		}
	}
	

	class Hori4Scenario_FallingRotation extends StripeMovementWidthScenario {

		@Override
		void initBehavs() {
			super.initBehavs();

			for (int i = 0; i < horiStripesDesCount; i++) {
				int rNum = (int) random(owner.stripes.length);
				Stripe randomStripe = owner.stripes[rNum];
				HoriStripe h = new HoriStripe(owner, randomStripe);
				h.yPos -= owner.heightOffset;
				owner.horiStripes.add(h);
			}
		}

		void onBehave() {
			super.onBehave();

			for (int i = 0; i < owner.horiStripes.size(); i++) {
				HoriStripe h = owner.horiStripes.get(i);

				if (random(100) > 90) {
					h.targetRotation = random(360 * 2) - 360;
				}

				if (random(100) > 90) {
					h.targetRotation = 0;
				}

				if (random(100) > 90)
					h.yStep = random(1000) / 200;
				h.update();
			}

		}
	}


	class Hori5Scenario_Randomizing extends StripeMovementWidthScenario {		
		HoriStripeBehRandomizingMovement beh;
		
		@Override
		void initBehavs() {
			super.initBehavs();
			beh = (HoriStripeBehRandomizingMovement) new HoriStripeBehRandomizingMovement().init(owner);
		}
		
		void onBehave() {
			super.onBehave();
			beh.onBehave();
		}
	}
		
	abstract class BaseHoriScenario extends StripeMovementWidthScenario {
		HashMap<HoriStripeBeh, Boolean> horiStripeBehs = new HashMap<HoriStripeBeh, Boolean>  ();
		
		@Override
		void initBehavs() {
			super.initBehavs();
		}

		@Override
		void onBehave() {
			super.onBehave();

			for (HoriStripeBeh beh : horiStripeBehs.keySet()) {
				if (horiStripeBehs.get(beh)) {
					beh.onBehave();
				}
			}
		}
	}
	
	class SuperHoriScenario extends BaseHoriScenario {
		Timer t = new Timer ((int)(500 + random (500)));
		
		@Override
		void initBehavs() {
			super.initBehavs();
			
			this.horiStripeBehs.put(new HoriStripeBehStop().init(this.owner), true);
			this.horiStripeBehs.put(new HoriStripeBehFalling().init(this.owner), true);
			this.horiStripeBehs.put(new HoriStripeBehRotation().init(this.owner), true);
			this.horiStripeBehs.put(new HoriStripeBehRandomizingMovement().init(this.owner), true);
		}
		
		@Override
		void onBehave() {
			super.onBehave();
			
			if (t.isTime()) {
				t = new Timer ((int)(500 + random (500)));
				
				if (random (100) > 70) {
					for (HoriStripeBeh beh : horiStripeBehs.keySet()) {					
						horiStripeBehs.put(beh, random (100) > 50);
						boolean b = horiStripeBehs.get (beh);
						if (b) break;
					}
				} else {
					for (HoriStripeBeh beh : horiStripeBehs.keySet()) {					
						horiStripeBehs.put(beh, random (100) > 70);
						boolean b = horiStripeBehs.get (beh);
						if (b) break;
					}
				}
				
			}
		}
	}

	class StripeDeplicationScenario extends StripeScenario {
		void initBehavs() {
		}

		void onBehave() {
			owner.widthOffset = lerp(owner.widthOffset, width / 2, 0.15f);
			baseStripe.myWidth = lerp(baseStripe.myWidth, 2f, 0.05f);
			owner.distModif = lerp(owner.distModif, 0f, 0.15f);
		}
	}

	abstract class StripeScenario {
		StripeController owner;

		ArrayList<StripeBehaviour> stripeBehavs = new ArrayList<StripeBehaviour>();

		StripeScenario onStart(StripeController stripeController) {
			this.owner = stripeController;
			stripeBehavs.clear();
			initBehavs();

			return this;
		}

		void initBehavs() {
		}

		void behave() {
			onBehave();

			for (StripeBehaviour b : stripeBehavs) {
				b.behave();
			}
		}

		void onBehave() {
		}
	}
}
