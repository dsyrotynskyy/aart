package aart1;

import aart1.Aart1.HoriStripe;
import aart1.Aart1.StripeController;

abstract class HoriStripeBeh {
	StripeController owner;
	
	HoriStripeBeh init (StripeController owner) {
		this.owner = owner;
		return this;
	}
	abstract void onBehave ();
}

class HoriStripeBehCreation extends HoriStripeBeh {
	Timer t = new Timer(100);
	int horiStripesDesCount = 0;
	
	@Override
	public void onBehave() {
	}		
}

class HoriStripeBehStop extends HoriStripeBeh {
	@Override
	public void onBehave() {
	}		
}

class HoriStripeBehFalling extends HoriStripeBeh {
	@Override
	public void onBehave() {
		for (int i = 0; i < owner.horiStripes.size(); i++) {
			HoriStripe h = owner.horiStripes.get(i);
			if (owner.owner.random(100) > 90)
				h.yStep = owner.owner.random(1000) / 200;
			h.update();

			if (owner.owner.random(100) > 90) {
				h.targetRotation = 0;
			}
		}
	}		
}

class HoriStripeBehRotation extends HoriStripeBeh {
	@Override
	public void onBehave() {
		for (int i = 0; i < owner.horiStripes.size(); i++) {
			HoriStripe h = owner.horiStripes.get(i);

			if (owner.owner.random(100) > 90) {
				h.targetRotation = owner.owner.random(360 * 2) - 360;
			}

			if (owner.owner.random(100) > 90) {
				h.targetRotation = 0;
			}

			h.update();
		}
	}		
}

class HoriStripeBehRandomizingMovement extends HoriStripeBeh {
	@Override
	public void onBehave() {
		for (int i = 0; i < owner.horiStripes.size(); i++) {
			HoriStripe h = owner.horiStripes.get(i);
			if (owner.owner.random(100) > 90) {
				h.yStep = owner.owner.random(1000) / 100 - 5;
				h.xStep = owner.owner. random(1000) / 100 - 5;
			}

			h.update();
		}
		
	}		
}
